Installation
============

You have to options to obtain the application: `Clone the repository`_ or `Download the latest release snapshot`_.


Clone the repository
--------------------

Clone the repository via: ``git clone https://gitlab.com/IPMsim/DataAnalysis.git``.


Download the latest release snapshot
------------------------------------

1. Download as

   * `tar.gz <https://gitlab.com/IPMsim/DataAnalysis/repository/archive.tar.gz?ref=master>`_
   * `zip <https://gitlab.com/IPMsim/DataAnalysis/repository/archive.zip?ref=master>`_
2. Unpack the archive: ``tar xzf <archive>`` or ``unzip <archive>``.


Install the requirements
------------------------

1. Navigate to the directory which you just obtained: ``cd DataAnalysis``.
2. On Unix you can simply do ``pip install -r requirements.txt``. On Windows you have to install
   ``matplotlib``, ``numpy`` and ``scipy`` manually. You might want to have a look at the
   `scipy installation instructions`_ or directly go for a fully-fledged Python platform such
   as `Anaconda`_.
3. You need to install PyQt (both PyQt4 and PyQt5 are supported; for Python 3.x only PyQt5 is supported).
   Please take a look at the `official PyQt website`_ for how to install it.

.. _scipy installation instructions: https://www.scipy.org/install.html
.. _Anaconda: https://www.continuum.io/downloads
.. _official PyQt website: https://www.riverbankcomputing.com/software/pyqt/download


Usage
=====

Navigate to the ``DataAnalysis`` directory and start the GUI via ``python start_xml_gui.py``.


How to use the GUI
------------------

* In the left upper corner there is a button that will open a file dialog which allows for
  selecting XML files that contain the data.
* After opening an XML file its DOM tree is displayed on the left, sorted by the four main
  categories `Meta`, `Device`, `Beam`, `ProfileSet`.
* When clicking a `Profile` or `Profile2D` element the corresponding data is plotted in the
  bottom right window.
* `Profile` and `Profile2D` elements can be dragged and dropped onto the upper right plot window,
  allowing for stacking an arbitrary number of profiles or adding/subtracting a profile from the others.
* Various data processing functions are available though a context menu (activate with right-click)
  such as normalizing or centering the profiles.
* Profiles in the right upper window can be fit with Gaussian distribution (this requires the
  packages ``scipy`` and/or `ROOT`_). This functionality is available through the context menu as well.

.. _ROOT: https://root.cern.ch/pyroot
