#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import sys

from data_analysis.gui import pyqt45
from data_analysis.gui.mainwindow import MainWindow

if __name__ == '__main__':
    app = pyqt45.Widgets.QApplication(sys.argv)

    view = MainWindow()
    view.show()

    sys.exit(app.exec_())
