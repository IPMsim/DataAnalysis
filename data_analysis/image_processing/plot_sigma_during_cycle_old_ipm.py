import sys

import matplotlib.pyplot as plt
import numpy as np

from data_analysis.tools.ipm_profiles import Profile1D


def get_rows_from_file(filepath, delim=' ', skip='#', skip_start=0, cast_to=str):
    with open(filepath) as fp:
        lines = fp.read().strip().split('\n')
        return [[cast_to(e) for e in l.split(delim)] for l in lines[skip_start:]
                if len(l) > 0 and not l.startswith(skip)]


def get_columns_from_file(*args, **kwargs):
    return zip(*get_rows_from_file(*args, **kwargs))


def get_sigma_from_data(positions, data):
    profile = Profile1D('', positions, data)
    profile.bring_to_zero()
    max_pos_x, _ = max(zip(positions, data), key=lambda x: x[1])
    xmin, xmax = max_pos_x - 10., max_pos_x + 10.
    chi2, sigma, mean, _ = profile.fit_gaussian_with_offset_scipy(x_min_max=(xmin, xmax))
    # if chi2 > 5.0:
    #     raise ValueError('Chi2 too large: %f' % chi2)
    return sigma


n_wires = 64

nrs, ys_h, ys_v = get_columns_from_file(sys.argv[1], delim='  ', skip_start=1, cast_to=float)

n_data_points = int(nrs[-1])

assert n_data_points % n_wires == 0, 'Number of values is not a multiple of number of wires'

n_profiles = n_data_points / n_wires
print 'number of profiles: %d' % n_profiles

ys_h, ys_v = np.array(ys_h), np.array(ys_v)
ys_h, ys_v = ys_h.reshape((n_data_points / n_wires, n_wires)), ys_v.reshape((n_data_points / n_wires, n_wires))

# positions = utils.get_positions_for_pixel_per_millimeter(13.240, n_wires)
positions = np.array(map(float, range(n_wires)))

sigmas_h, sigmas_v = [], []

# plt.plot(positions, ys_h[500], label='300')
# plt.plot(positions, ys_h[600], label='400')
# plt.plot(positions, ys_h[700], label='500')

start_at = 75

nrs_h = range(n_profiles)[start_at:]
nrs_v = range(n_profiles)[start_at:]

for nr_h, nr_v, data_h, data_v in zip(nrs_h, nrs_v, ys_h[start_at:], ys_v[start_at:]):
    # plt.plot(positions, data_h)
    # try:
    #     sigmas_h.append(get_sigma_from_data(positions, data_h))
    # except RuntimeError:
    #     break
    # plt.plot(positions, data_v)
    # try:
    #     sigmas_v.append(get_sigma_from_data(positions, data_v))
    # except RuntimeError:
    #     break
    try:
        sigmas_h.append(get_sigma_from_data(positions, data_h))
    except (RuntimeError, ValueError) as err:
        print '[horizontal] ' + err.message
        nrs_h.remove(nr_h)
    try:
        sigmas_v.append(get_sigma_from_data(positions, data_v))
    except (RuntimeError, ValueError) as err:
        print '[vertical] ' + err.message
        nrs_v.remove(nr_v)

plt.plot(nrs_h, sigmas_h, 'bo', label='horizontal')
plt.plot(nrs_v, sigmas_v, 'rs', label='vertical')
plt.legend()
plt.show()
