import datetime
import glob
import itertools
import logging
import numpy as np
import os
import png
import re
import shutil
import sys
import time

from data_analysis.preparation.converters.xml_utils import create_xml_2d, save_profile_set_with_template

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

assert len(sys.argv) == 7, 'Not enough arguments'

directory = sys.argv[1]
start_time = sys.argv[2]
stop_time = sys.argv[3]
destination_dir = sys.argv[4]
xml_template_file = sys.argv[5]
orientation = sys.argv[6].lower()

print 'Source directory: %s' % directory
print 'Start time: %s' % start_time
print 'Stop time: %s' % stop_time
print 'Destination directory: %s' % destination_dir
print 'XML template file: %s' % xml_template_file
print 'Orientation: %s' % orientation
choice = raw_input('Ok? [Y/n] ')
# choice = 'Y'
if choice != 'Y':
    print 'quit'
    quit()

try:
    os.mkdir(destination_dir)
except OSError:
    logging.debug('%s already exists' % destination_dir)

image_dir = 'images'
destination_image_dir = os.path.join(destination_dir, image_dir)
try:
    os.mkdir(destination_image_dir)
except OSError:
    logging.debug('%s already exists' % destination_image_dir)

final_xml_filename = 'output.xml'

datetime_format = "%Y-%m-%d %H:%M:%S"


def get_datetime_string_and_millis_from_filename(filename):
    head, tail = os.path.split(filename)
    pattern = r'ipmImg-(?P<date>[0-9]+)_(?P<time>[0-9]+)_(?P<millis>[0-9]+)_Cam_(0|1)\.png'
    match = re.match(pattern, tail)
    if match is None:
        raise ValueError('Datetime regex did not match for %s' % tail)
    gd = match.groupdict()
    datetime_string = '%s-%s-%s %s:%s:%s' % (gd['date'][:4], gd['date'][4:6], gd['date'][6:8],
                                             gd['time'][:2], gd['time'][2:4], gd['time'][4:6])
    return datetime_string, gd['millis']


def convert_filename_to_datetime(filename):
    datetime_string, _ = get_datetime_string_and_millis_from_filename(filename)
    return datetime.datetime.strptime(datetime_string, datetime_format)


def convert_filename_to_datetime_and_millis(filename):
    datetime_string, millis = get_datetime_string_and_millis_from_filename(filename)
    return datetime.datetime.strptime(datetime_string, datetime_format), millis


def convert_filename_to_unix_timestamp(filename):
    datetime_string, millis = get_datetime_string_and_millis_from_filename(filename)
    return time.mktime(datetime.datetime.strptime(datetime_string, datetime_format).timetuple()) + float(millis)/1000.


start = datetime.datetime.strptime(start_time, datetime_format)
stop = datetime.datetime.strptime(stop_time, datetime_format)


def filter_by_time(filename):
    return start <= convert_filename_to_datetime(filename) < stop


images = filter(filter_by_time, glob.glob(os.path.join(directory, '*.png')))

# need to take vertical camera as cycle trigger because signal on horizontal is too bad
orientation_id = 'Cam_1'
images = filter(lambda x: orientation_id in x, images)
images = sorted(images, key=convert_filename_to_unix_timestamp)
logging.debug('Filtering %d images' % len(images))


def single_profile_has_minimum_peak_height(profile):
    diff = 25
    return np.max(profile.profile) - profile[0] >= diff and np.max(profile.profile) - profile[-1] >= diff


conditions_single_profile = [single_profile_has_minimum_peak_height]


def profile_set_spans_minimum_time(profile_set):
    if len(profile_set) == 0:
        return False
    time_diff = 6
    start_time = convert_filename_to_datetime(profile_set[0].filename)
    stop_time = convert_filename_to_datetime(profile_set[-1].filename)
    return stop_time - start_time >= datetime.timedelta(seconds=time_diff)


def profile_set_has_minimum_number_of_profiles(profile_set):
    return len(profile_set) >= 6


conditions_profile_set = [profile_set_spans_minimum_time, profile_set_has_minimum_number_of_profiles]


def satisfies_all_conditions(item, conditions):
    return all(map(lambda x: x[1](x[0]), itertools.product([item], conditions)))


roi_left, roi_top, roi_right, roi_bottom = 60, 245, 1245, 780


def get_pixels_from_png(filename):
    image = png.Reader(filename)
    width, height, pixels, meta = image.read()
    return np.asarray(list(pixels))[roi_top: roi_bottom, roi_left: roi_right]


def average_image_to_profile(pixels):
    return np.sum(pixels, axis=0) / pixels.shape[0]


class Profile:
    def __init__(self, filename, profile):
        self.filename = filename
        self.profile = profile

    def __getitem__(self, item):
        return self.profile[item]

all_image_filenames = []
all_unix_timestamps = []


def cycle_detected(start_filename):
    orientation_id = {'horizontal': 'Cam_0', 'vertical': 'Cam_1'}[orientation]

    start_timestamp = convert_filename_to_datetime(start_filename)
    start_flattop_timestamp = start_timestamp + datetime.timedelta(seconds=3)
    end_flattop_timestamp = start_flattop_timestamp + datetime.timedelta(seconds=2)

    images = glob.glob(os.path.join(directory, '*.png'))
    images = filter(lambda filename: orientation_id in filename, images)
    images = filter(
        lambda filename: (start_flattop_timestamp <= convert_filename_to_datetime(filename) < end_flattop_timestamp),
        images)
    images = sorted(images, key=convert_filename_to_unix_timestamp)

    flattop_files = images

    unix_times = []
    filenames = []
    for filepath in flattop_files:
        head, tail = os.path.split(filepath)
        logging.debug('%s cp-> %s' % (filepath, os.path.join(destination_image_dir, tail)))
        shutil.copy(filepath, os.path.join(destination_image_dir, tail))

        all_unix_timestamps.append(convert_filename_to_unix_timestamp(filepath))
        all_image_filenames.append(os.path.join(image_dir, tail))


log_every = len(images) / 100
profile_set = []
cycle_detected_flag = False
for i, filename in enumerate(images):
    # if i % log_every == 0:
    #     logging.debug('Now processing: %s' % filename)
    profile = Profile(filename, average_image_to_profile(get_pixels_from_png(filename)))
    if satisfies_all_conditions(profile, conditions_single_profile):
        logging.debug('[OK] Profile %s' % filename)
        profile_set.append(profile)
        if not cycle_detected_flag and satisfies_all_conditions(profile_set, conditions_profile_set):
            logging.debug(
                '[CYCLE] Cycle detected: (%s, %s)' % (str(convert_filename_to_datetime(profile_set[0].filename)),
                                                      str(convert_filename_to_datetime(profile_set[-1].filename))))
            cycle_detected_flag = True
            cycle_detected(profile_set[0].filename)
    else:
        logging.debug('[X] Profile %s' % filename)
        profile_set = []
        cycle_detected_flag = False

xml_code = create_xml_2d(all_image_filenames, all_unix_timestamps)
save_profile_set_with_template(os.path.join(destination_dir, final_xml_filename), xml_template_file, xml_code)
