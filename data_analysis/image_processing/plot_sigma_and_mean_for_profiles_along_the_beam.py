import sys

import matplotlib.pyplot as plt
import numpy as np

import utils
from data_analysis.tools.ipm_profiles import Profile1D

assert len(sys.argv) == 5, 'Not enough arguments'

filepath = sys.argv[1]
roi_l_edge = int(sys.argv[2])
roi_r_edge = int(sys.argv[3])
what = sys.argv[4]

if roi_r_edge < 0:
    pixels = utils.get_pixels_from_png(filepath)
else:
    pixels = utils.get_pixels_from_png_with_region_of_interest(filepath, roi_l_edge, roi_r_edge)
print 'Image size: %dx%d' % (pixels.shape[0], pixels.shape[1])
n_rows = pixels.shape[0]

positions = utils.get_positions_for_pixel_per_millimeter(13.240, pixels.shape[1])
# positions = utils.get_positions_for_pixel_per_millimeter(1, pixels.shape[1])
# positions = np.array(range(pixels.shape[1]), dtype=float)

chi2s = []
sigmas = []
means = []
means_fit = []
peak_positions = []

chi2s_2 = []
sigmas_2 = []

pixel_numbers = list((np.array(range(n_rows)) + roi_l_edge).tolist())
pixel_numbers_2 = list((np.array(range(n_rows)) + roi_l_edge).tolist())

lower_edges = range(n_rows)
upper_edges = lower_edges[1:] + [n_rows]

for pixel_number, l_edge, u_edge in zip(pixel_numbers, lower_edges, upper_edges):
    averaged_profile = utils.average_2d_profile(pixels[l_edge : u_edge])
    profile = Profile1D('%d-%d' % (l_edge, roi_r_edge), positions, averaged_profile)
    profile.bring_to_zero()
    try:
        chi2, sigma, mean, fit_xs, fit_ys = profile.fit_gaussian_plus_polynomial_scipy(degree=1, x_min_max=(30., 70.))
        # chi2, sigma, mean, fit_xs, fit_ys = profile.fit_gaussian_with_offset_scipy(x_min_max=(30., 70.))
        # chi2, sigma, mean, fit_xs, fit_ys = profile.fit_gaussian_with_offset_root(x_min_max=(30., 70.))
    except ValueError as err:
        print err.message
        pixel_numbers.remove(pixel_number)
    else:
        chi2s.append(chi2)
        sigmas.append(abs(sigma))
        means.append(profile.mean)
        means_fit.append(mean)
        peak_positions.append(profile.peak_position)

    try:
        chi2, sigma, mean, fit_xs, fit_ys = profile.fit_gaussian_with_offset_scipy(x_min_max=(30., 70.))
    except ValueError as err:
        print err.message
        pixel_numbers_2.remove(pixel_number)
    else:
        chi2s_2.append(chi2)
        sigmas_2.append(abs(sigma))

plt.xlabel('Image row (counting from top) [pixel]')

if what == 'chi2':
    plt.plot(pixel_numbers, chi2s, 'bo', label='chi2')
    plt.title('Chi square of fit along IPM image (using scipy fit)')
    plt.ylabel('Chi2')
    # plt.ylabel('Chi2 [1/dof]')

    plt.plot(pixel_numbers_2, chi2s_2, 'rx', label='chi2 offset only')
elif what == 'sigma':
    plt.plot(pixel_numbers, sigmas, 'bo', label='sigma')
    plt.title('Estimation of profile sigma along IPM image (using scipy fit)')
    plt.ylabel('Sigma [mm]')

    plt.plot(pixel_numbers_2, sigmas_2, 'rx', label='sigma offset only')
elif what == 'mean':
    plt.plot(pixel_numbers, means_fit, 'bo', label='mean (fit)')
    plt.plot(pixel_numbers, means, 'r-', label='mean')
    plt.plot(pixel_numbers, peak_positions, 'g-', label='peak')
    plt.title('Estimations of profile center along IPM image')
    plt.ylabel('Profile center [mm]')
else:
    raise ValueError('Invalid specifier: %s' % what)
plt.legend()
plt.show()
