import sys

import matplotlib.pyplot as plt

import utils
from data_analysis.tools.ipm_profiles import Profile1D

assert len(sys.argv) == 4, 'Not enough arguments'

filepath = sys.argv[1]
roi_l_edge = int(sys.argv[2])
roi_r_edge = int(sys.argv[3])

if roi_r_edge < 0:
    pixels = utils.get_pixels_from_png(filepath)
else:
    pixels = utils.get_pixels_from_png_with_region_of_interest(filepath, roi_l_edge, roi_r_edge)
print 'Image size: %dx%d' % (pixels.shape[0], pixels.shape[1])
n_rows = pixels.shape[0]

positions = utils.get_positions_for_pixel_per_millimeter(13.240, pixels.shape[1])

averaged_profile = utils.average_2d_profile(pixels)

profile = Profile1D('Profile', positions, averaged_profile)
profile.bring_to_zero()
# chi2, sigma, mean, fit_xs, fit_ys = profile.fit_gaussian_plus_polynomial_scipy(degree=1, x_min_max=(30., 70.), log_to_stdout=True)
chi2, sigma, mean, fit_xs, fit_ys = profile.fit_gaussian_with_offset_scipy(x_min_max=(30., 70.), log_to_stdout=True)
# chi2, sigma, mean, fit_xs, fit_ys = profile.fit_gaussian_with_offset_root(x_min_max=(30., 70.), log_to_stdout=True)

plt.plot(profile.xs, profile.ys, 'bo', label='Profile')
plt.plot(fit_xs, fit_ys, 'r-', label='Fit')

plt.xlabel('Position [mm]')
plt.ylabel('Amplitude [a.u.]')
plt.legend()
plt.show()
