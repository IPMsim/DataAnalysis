import matplotlib.pyplot as plt
import numpy as np
import utils
import sys

filepath = sys.argv[1]
roi_l_edge = int(sys.argv[2])
roi_r_edge = int(sys.argv[3])
n_profiles = int(sys.argv[4])

if roi_r_edge < 0:
    pixels = utils.get_pixels_from_png(filepath)
else:
    pixels = utils.get_pixels_from_png_with_region_of_interest(filepath, roi_l_edge, roi_r_edge)
print 'Image size: %dx%d' % (pixels.shape[0], pixels.shape[1])
n_rows = pixels.shape[0]

if n_rows % n_profiles > 0:
    print (
    'Number of rows is not a multiple of number of profiles: %d rows per profile, %d rows for the last profile' % (
    n_rows / n_profiles, n_rows % n_profiles))

lower_edges = int(n_rows / n_profiles) * np.array(range(n_profiles))
upper_edges = np.insert(lower_edges[1:], n_profiles - 1, n_rows)

positions = utils.get_positions_for_pixel_per_millimeter(13.240, pixels.shape[1])

for l_edge, u_edge in zip(lower_edges, upper_edges):
    averaged_profile = utils.average_2d_profile(pixels[l_edge : u_edge])
    plt.plot(positions, averaged_profile, label='%d-%d' % (l_edge, u_edge))

plt.legend()
plt.show()
