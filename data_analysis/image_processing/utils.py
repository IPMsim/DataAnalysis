import numpy as np
import png


def get_pixels_from_png(filepath):
    image = png.Reader(str(filepath))
    width, height, pixels, meta = image.read()
    return np.asarray(list(pixels))


def get_pixels_from_png_with_region_of_interest(filepath, l_edge, u_edge):
    return get_pixels_from_png(filepath)[l_edge : u_edge + 1]


def average_2d_profile(profile):
    return np.sum(profile, axis=0) / float(profile.shape[0])


def get_positions_for_pixel_per_millimeter(px_p_mm, n_pixels):
    return 1./px_p_mm * np.array(range(n_pixels))
