# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import re
import time


def get_datetime_from_iso_datetime_string(dt_string):
    return dt_string.replace('T', ' ')


def get_time_from_iso_datetime_string(dt_string):
    regex_format = r"(?P<year>[0-9]+)-(?P<month>[0-9]+)-(?P<day>[0-9]+)T" \
                   + r"(?P<hours>[0-9]+):(?P<minutes>[0-9]+):(?P<seconds>[0-9]+).(?P<milliseconds>[0-9]+)?"
    match = re.match(
        r"(?P<year>[0-9]+)-(?P<month>[0-9]+)-(?P<day>[0-9]+)"
        r"T(?P<hours>[0-9]+):(?P<minutes>[0-9]+):(?P<seconds>[0-9]+)(\.(?P<milliseconds>[0-9]+))?",
        dt_string
    )
    gd = match.groupdict()
    time_string = '%s:%s:%s' % (gd['hours'], gd['minutes'], gd['seconds'])
    if gd['milliseconds'] is not None:
        time_string += '.%s' % gd['milliseconds']
    return time_string


def get_datetime_from_unix_timestamp(timestamp):
    timestamp = float(timestamp)
    millis = int('%3.0f' % (1000 * (timestamp - int(timestamp))))
    return '%s.%d' % (time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(timestamp)), millis)


def get_time_from_unix_timestamp(timestamp):
    timestamp = float(timestamp)
    millis = int('%3.0f' % (1000 * (timestamp - int(timestamp))))
    time_string = time.strftime("%H:%M:%S", time.gmtime(timestamp))
    return '%s.%03d' % (time_string, millis)
