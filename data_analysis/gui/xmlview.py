# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, unicode_literals

import os.path
import re
import time
import xml.etree.ElementTree as XMLET

import numpy as np
import six

from . import pyqt45
from . import log
from . import utils

QtCore = pyqt45.QtCore
Widgets = pyqt45.Widgets


class XMLElement:
    def __init__(self, node, row, parent=None):
        self._node = node
        self._row = row
        self._parent = parent
        self.child_items = {}

    def node(self):
        return self._node

    def parent(self):
        return self._parent

    def row(self):
        return self._row

    def child(self, index):
        if index in self.child_items:
            return self.child_items[index]

        try:
            child_node = self._node.getchildren()[index]
        except IndexError:
            return None
        else:
            child_item = XMLElement(child_node, index, self)
            self.child_items[index] = child_item
            return child_item


class XMLElementModel(QtCore.QAbstractItemModel):
    def __init__(self, xml_element, parent=None):
        super(XMLElementModel, self).__init__(parent)

        self.root = xml_element

    def data(self, index, role=None):
        if not index.isValid():
            return QtCore.QVariant()
        if role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()

        item = index.internalPointer()
        attributes = item.node().attrib

        columns = {
            0: item.node().tag,
            1: '' if item.node().text is None else (item.node().text + (' %s' % attributes[
                'unit'] if 'unit' in attributes and attributes['unit'] != '1' else '')),
            2: ' '.join(['%s="%s"' % (str(k), str(v)) for k, v in iter(attributes.items())]),
        }

        if str(item.node().tag) == 'Contact':
            name = item.node().find('Name')
            if name is not None:
                columns[1] = name.text

        elif str(item.node().tag) == 'Type':
            if item.node().getchildren():
                columns[1] = item.node().getchildren()[0].tag

        elif str(item.node().tag) == 'Timestamp':
            try:
                t_format = item.node().attrib['format']
            except KeyError:
                t_format = 'unix'

            if t_format == 'unix':
                columns[1] = utils.get_datetime_from_unix_timestamp(item.node().text) + (' (%s)' % columns[1])
            elif t_format == 'iso':
                columns[1] = utils.get_datetime_from_iso_datetime_string(item.node().text) + (' (%s)' % columns[1])

        elif str(item.node().tag) in ('Profile', 'Profile2D'):
            timestamp = item.node().find('Timestamp')
            if timestamp is None:
                print('No timestamp available')
            else:
                try:
                    t_format = timestamp.attrib['format']
                except KeyError:
                    t_format = 'unix'

                if t_format == 'unix':
                    columns[1] = utils.get_time_from_unix_timestamp(item.node().find('Timestamp').text)
                elif t_format == 'iso':
                    columns[1] = utils.get_time_from_iso_datetime_string(item.node().find('Timestamp').text)

        return columns[index.column()]

    def flags(self, index):
        if not index.isValid():
            return 0
        default_flags = QtCore.QAbstractItemModel.flags(self, index)
        node = index.internalPointer().node()
        if node.tag == 'Profile' or node.tag == 'Profile2D':
            # return QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled | default_flags
            return QtCore.Qt.ItemIsDragEnabled | default_flags
        else:
            return default_flags

    def headerData(self, section, orientation, role=None):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            headers = {
                0: 'Name',
                1: 'Value',
                2: 'Attributes',
            }
            try:
                return headers[section]
            except KeyError:
                return QtCore.QVariant()
        return QtCore.QVariant()

    def index(self, row, col, parent=None, *args, **kwargs):
        if not self.hasIndex(row, col, parent):
            return QtCore.QModelIndex()
        parent_item = self.root if not parent.isValid() else parent.internalPointer()
        return self.createIndex(row, col, parent_item.child(row)) if parent_item.child(row) else QtCore.QModelIndex()

    def parent(self, child=None):
        if not child.isValid():
            return QtCore.QModelIndex()
        child_item = child.internalPointer()
        parent_item = child_item.parent()
        if not parent_item or parent_item is self.root:
            return QtCore.QModelIndex()
        return self.createIndex(parent_item.row(), 0, parent_item)

    def rowCount(self, parent=None, *args, **kwargs):
        parent_item = self.root if not parent.isValid() else parent.internalPointer()

        return len(parent_item.node().getchildren())

    def columnCount(self, QModelIndex_parent=None, *args, **kwargs):
        return 3


class XMLElementView(Widgets.QTreeView):
    def __init__(self, path_options=(), parent=None):
        super(XMLElementView, self).__init__(parent)

        self.setDragEnabled(True)
        if isinstance(path_options, six.text_type):
            path_options = (path_options,)
        self._path_options = path_options
        self._parent = parent

        self.setMinimumHeight(300)

    def new_model(self, root_node):
        dest_node = None
        for option in self._path_options:
            dest_node = root_node.find(option)
            if dest_node is not None:
                break
            print("Couldn't find option %s" % option)
        if dest_node is None:
            return
        root_element = XMLElement(dest_node, 0)
        self.model = XMLElementModel(root_element)
        self.setModel(self.model)

        if self._parent is not None:
            self.setSelectionModel(QtCore.QItemSelectionModel(self.model))
            self.selectionModel().currentChanged.connect(
                self._parent.plot_mini_wrapper_for_selection_model
            )


class XMLView(Widgets.QWidget):
    def __init__(self, miniplot, parent=None):
        super(XMLView, self).__init__(parent)

        log.logging.debug('XMLView.__init__')

        self.miniplot = miniplot

        self.xml_view_selector = Widgets.QComboBox()

        self.xml_view = Widgets.QStackedWidget()
        self.xml_views_by_name = {}

        def add_xml_view(title, xml_path, parent_view=None):
            self.xml_view_selector.addItem(title)
            xml_element_view = XMLElementView(xml_path, parent_view)
            self.xml_view.addWidget(xml_element_view)
            self.xml_views_by_name[title] = xml_element_view
            return xml_element_view

        self.xml_view_meta = add_xml_view('Meta', 'Meta')
        self.xml_view_device = add_xml_view('Device', 'Device')
        self.xml_view_beam = add_xml_view('Beam', ('Beams', 'Beam'))
        self.xml_view_profiles = add_xml_view('Profiles', 'ProfileSet', self)

        v_layout = Widgets.QVBoxLayout()

        container = Widgets.QWidget()
        container_layout = Widgets.QVBoxLayout()
        container_layout.addWidget(self.xml_view_selector)
        container_layout.addWidget(self.xml_view)
        container.setLayout(container_layout)
        v_layout.addWidget(container)

        self.setLayout(v_layout)

        self.xml_view_selector.currentIndexChanged.connect(self.change_current_xml_view)

        log.logging.debug('XMLView.__init__ -> done')

    def open_file(self, filepath):
        self._open_filepath = filepath
        root_node = XMLET.parse(filepath).getroot()
        self.xml_view_meta.new_model(root_node)
        self.xml_view_device.new_model(root_node)
        self.xml_view_beam.new_model(root_node)
        self.xml_view_profiles.new_model(root_node)

    def change_current_xml_view(self):
        self.xml_view.setCurrentWidget(
            self.xml_views_by_name[str(self.xml_view_selector.itemText(self.xml_view_selector.currentIndex()))])

    def plot_mini_wrapper_for_selection_model(self, index, _):
        tag = self.xml_view_profiles.currentIndex().internalPointer().node().tag
        if tag == 'Profile':
            self.plot_mini()
        elif tag == 'Profile2D':
            self.plot_mini_2d()

    def plot_mini(self):
        xs, ys = self.get_current_profile()
        if len(xs) > 0:
            self.miniplot.plot_1d(xs, ys)

    def plot_mini_2d(self):
        filename = self.get_current_profile_2d()
        self.miniplot.plot_png(os.path.join(os.path.split(self._open_filepath)[0], filename))

    def get_current_tag(self):
        return self.xml_view_profiles.currentIndex().internalPointer().node().tag

    def get_positions_of_current_profile2d(self):
        node = self.xml_view_profiles.currentIndex().internalPointer().node()
        return self.get_positions_from_profile2d_position_generator(str(node.find('Position').text))

    def get_current_profile2d_filepath(self):
        node = self.xml_view_profiles.currentIndex().internalPointer().node()
        filename = str(node.find('Filename').text)
        return os.path.join(os.path.split(self._open_filepath)[0], filename)

    def get_current_profile(self):
        node = self.xml_view_profiles.currentIndex().internalPointer().node()
        tag = node.tag
        if not tag == 'Profile':
            return [], []

        amplitude = node.find('Amplitude')
        ys = np.array(list(map(float, amplitude.text.split(','))))
        xs = self.get_positions_from_text(node.find('Position').text, len(ys))

        return xs, ys

    def get_current_profile_2d(self):
        node = self.xml_view_profiles.currentIndex().internalPointer().node()
        return str(node.find('Filename').text)

    @staticmethod
    def get_positions_from_text(text, points_per_profile):
        # TODO: use regex matching to see which format the positions are given in
        # TODO: move separator (',') to some variable
        if ',' in text:
            xs = np.array(list(map(float, text.split(','))))
        else:
            step, offset = float(text.split('*')[0].strip()), float(text.split('+')[-1].strip())
            xs = step * np.array(list(range(points_per_profile))) + offset
        return xs

    @staticmethod
    def get_positions_from_profile2d_position_generator(generator):
        fpn_pattern = '[\+\-]*[0-9]+[.]*[0-9]*'  # fpn = floating point number
        int_pattern = '[0-9]+'  # int = integer (only positive)
        pattern = '(?P<%s>%s)\*i \+ (?P<%s>%s) \((?P<%s>%s)\)' % ('step', fpn_pattern, 'offset', fpn_pattern,
                                                                  'n_points', int_pattern)
        match = re.match(pattern, generator)
        step = float(match.groupdict()['step'])
        offset = float(match.groupdict()['offset'])
        n_points = int(match.groupdict()['n_points'])
        return step * np.array(range(n_points)) + offset
