# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from matplotlib.figure import Figure
import numpy as np
import png

from . import pyqt45

FigureCanvas = pyqt45.matplotlib_backend_qtXagg.FigureCanvasQTAgg

QtCore = pyqt45.QtCore
Widgets = pyqt45.Widgets


class DropProfileDialog(Widgets.QDialog):
    def __init__(self, parent=None):
        super(DropProfileDialog, self).__init__(parent)

        self.setWindowTitle('What to do?')

        g_layout_scaling_factor = Widgets.QGridLayout()
        g_layout_scaling_factor.addWidget(
            Widgets.QLabel('x-axis scaling factor:'),
            0, 0
        )
        g_layout_scaling_factor.addWidget(
            Widgets.QLabel('y-axis scaling factor:'),
            1, 0
        )
        g_layout_scaling_factor.addWidget(
            Widgets.QLabel('(numpy functions available through `np`)'),
            2, 0, 1, 2
        )

        self.x_axis_scaling_factor = Widgets.QLineEdit()
        # self.x_axis_scaling_factor.setValidator(Widgets.QDoubleValidator())
        self.x_axis_scaling_factor.setText('1.0')
        g_layout_scaling_factor.addWidget(self.x_axis_scaling_factor, 0, 1)

        self.y_axis_scaling_factor = Widgets.QLineEdit()
        # self.y_axis_scaling_factor.setValidator(Widgets.QDoubleValidator())
        self.y_axis_scaling_factor.setText('1.0')
        g_layout_scaling_factor.addWidget(self.y_axis_scaling_factor, 1, 1)

        self.choices_group = Widgets.QButtonGroup()
        self.choices_group_view = Widgets.QGroupBox('Actions')

        choices_group_view_layout = Widgets.QVBoxLayout()

        g_layout = Widgets.QGridLayout()

        button = Widgets.QRadioButton('stack')
        self.choices_group.addButton(button)
        g_layout.addWidget(button, 0, 0)

        self.plot_name = Widgets.QLineEdit()
        self.plot_name.setPlaceholderText('plot name')
        g_layout.addWidget(self.plot_name, 0, 1, 1, 2)

        self.plot_color = Widgets.QComboBox()
        self.plot_color.addItem('blue', 'b')
        self.plot_color.addItem('green', 'g')
        self.plot_color.addItem('red', 'r')
        self.plot_color.addItem('cyan', 'c')
        self.plot_color.addItem('magenta', 'm')
        self.plot_color.addItem('yellow', 'y')
        self.plot_color.addItem('black', 'k')

        g_layout.addWidget(Widgets.QLabel('color'), 1, 1)
        g_layout.addWidget(self.plot_color, 1, 2)

        self.plot_linestyle = Widgets.QComboBox()
        self.plot_linestyle.addItem('solid', '-')
        self.plot_linestyle.addItem('dashed', '--')
        self.plot_linestyle.addItem('dash-dot', '-.')
        self.plot_linestyle.addItem('dotted', ':')

        g_layout.addWidget(Widgets.QLabel('line style'), 2, 1)
        g_layout.addWidget(self.plot_linestyle, 2, 2)

        self.plot_marker = Widgets.QComboBox()
        self.plot_marker.addItem('None', None)
        self.plot_marker.addItem('point', '.')
        self.plot_marker.addItem('circle', 'o')
        self.plot_marker.addItem('triangle', '^')
        self.plot_marker.addItem('square', 's')
        self.plot_marker.addItem('star', '*')
        self.plot_marker.addItem('plus', '+')
        self.plot_marker.addItem('x', 'x')

        g_layout.addWidget(Widgets.QLabel('marker'), 3, 1)
        g_layout.addWidget(self.plot_marker, 3, 2)

        choices_group_view_layout.addLayout(g_layout)

        def add_choice(choice):
            button = Widgets.QRadioButton(choice)
            self.choices_group.addButton(button)
            choices_group_view_layout.addWidget(button)

        add_choice('add')
        add_choice('subtract')

        choices_group_view_layout.addStretch(1)

        self.choices_group_view.setLayout(choices_group_view_layout)

        self.choices_group.setExclusive(True)
        self.choices_group.buttons()[0].setChecked(True)

        v_layout = Widgets.QVBoxLayout()
        v_layout.addLayout(g_layout_scaling_factor)
        v_layout.addWidget(self.choices_group_view)

        ok_button = Widgets.QPushButton('ok')
        cancel_button = Widgets.QPushButton('cancel')

        button_layout = Widgets.QHBoxLayout()
        button_layout.addWidget(ok_button)
        button_layout.addWidget(cancel_button)

        ok_button.clicked.connect(self.accept)
        cancel_button.clicked.connect(self.reject)

        v_layout.addLayout(button_layout)

        self.setLayout(v_layout)

    def get_choice(self):
        return self.choices_group.checkedButton().text()

    def get_plot_name(self):
        return self.plot_name.text()

    def get_x_axis_scaling_factor(self):
        import numpy as np
        return float(eval(str(self.x_axis_scaling_factor.text())))

    def get_y_axis_scaling_factor(self):
        import numpy as np
        return float(eval(str(self.y_axis_scaling_factor.text())))

    def get_plot_style(self):
        marker = pyqt45.currentDataAsObject(self.plot_marker)
        if marker is not None:
            marker = str(marker)
        return {
            'color': str(pyqt45.currentDataAsObject(self.plot_color)),
            'linestyle': str(pyqt45.currentDataAsObject(self.plot_linestyle)),
            'marker': marker,
        }


class RegionOfInterestDialog(Widgets.QDialog):
    def __init__(self, parent=None):
        super(RegionOfInterestDialog, self).__init__(parent)

        self.setWindowTitle('Region of Interest')

        self.xmin_input = Widgets.QLineEdit()
        self.xmin_input.setValidator(Widgets.QDoubleValidator())
        self.xmax_input = Widgets.QLineEdit()
        self.xmax_input.setValidator(Widgets.QDoubleValidator())

        input_layout = Widgets.QGridLayout()
        input_layout.addWidget(Widgets.QLabel('xmin'), 0, 0)
        input_layout.addWidget(Widgets.QLabel('xmax'), 0, 1)
        input_layout.addWidget(self.xmin_input, 1, 0)
        input_layout.addWidget(self.xmax_input, 1, 1)

        ok_button = Widgets.QPushButton('ok')
        cancel_button = Widgets.QPushButton('cancel')

        button_layout = Widgets.QHBoxLayout()
        button_layout.addWidget(ok_button)
        button_layout.addWidget(cancel_button)

        ok_button.clicked.connect(self.accept)
        cancel_button.clicked.connect(self.reject)

        v_layout = Widgets.QVBoxLayout()
        v_layout.addLayout(input_layout)
        v_layout.addLayout(button_layout)
        self.setLayout(v_layout)

    def get_xmin_xmax(self):
        return float(self.xmin_input.text()), float(self.xmax_input.text())


# TODO: add drawing functionality
class RegionOfInterestDialog2DInteractive(Widgets.QDialog):
    def __init__(self, filepath, xs, parent=None):
        super(RegionOfInterestDialog2DInteractive, self).__init__(parent)

        self.xs = xs

        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setParent(self)
        self.canvas.setSizePolicy(Widgets.QSizePolicy.Expanding, Widgets.QSizePolicy.Expanding)
        self.canvas.updateGeometry()

        image = png.Reader(filepath)
        width, height, pixels, meta = image.read()
        self.pixels = np.asarray(list(pixels))
        self.axes.imshow(self.pixels, cmap='gray')
        self.axes.set_xlabel('x')
        self.axes.set_ylabel('y')
        self.canvas.draw()

        def setup_spin_box(sbox, max_val, current_val):
            sbox.setRange(0, max_val)
            sbox.setSingleStep(1)
            sbox.setValue(current_val)
            sbox.valueChanged.connect(self.update_region_of_interest)

        self.left_x = Widgets.QSpinBox()
        self.right_x = Widgets.QSpinBox()
        self.top_y = Widgets.QSpinBox()
        self.bottom_y = Widgets.QSpinBox()

        setup_spin_box(self.left_x, width, 0)
        setup_spin_box(self.right_x, width, width)
        setup_spin_box(self.top_y, height, 0)
        setup_spin_box(self.bottom_y, height, height)

        self.top_line = self.axes.plot(*self.top_line_coordinates, color='r')[0]
        self.right_line = self.axes.plot(*self.right_line_coordinates, color='r')[0]
        self.bottom_line = self.axes.plot(*self.bottom_line_coordinates, color='r')[0]
        self.left_line = self.axes.plot(*self.left_line_coordinates, color='r')[0]

        v_layout = Widgets.QVBoxLayout()

        h_layout = Widgets.QHBoxLayout()
        h_layout.addWidget(Widgets.QLabel('y-axis:'))
        h_layout.addStretch(1)
        v_layout.addLayout(h_layout)
        h_layout = Widgets.QHBoxLayout()
        h_layout.addWidget(Widgets.QLabel('top ='))
        h_layout.addWidget(self.top_y)
        h_layout.addWidget(Widgets.QLabel('bottom ='))
        h_layout.addWidget(self.bottom_y)
        v_layout.addLayout(h_layout)

        h_layout = Widgets.QHBoxLayout()
        h_layout.addWidget(Widgets.QLabel('x-axis:'))
        h_layout.addStretch(1)
        v_layout.addLayout(h_layout)
        h_layout = Widgets.QHBoxLayout()
        h_layout.addWidget(Widgets.QLabel('left ='))
        h_layout.addWidget(self.left_x)
        h_layout.addWidget(Widgets.QLabel('right ='))
        h_layout.addWidget(self.right_x)
        v_layout.addLayout(h_layout)

        v_layout.addStretch(1)

        h_layout = Widgets.QHBoxLayout()
        h_layout.addStretch(1)
        ok_button = Widgets.QPushButton('ok')
        ok_button.clicked.connect(self.accept)
        h_layout.addWidget(ok_button)
        cancel_button = Widgets.QPushButton('cancel')
        cancel_button.clicked.connect(self.reject)
        h_layout.addWidget(cancel_button)
        v_layout.addLayout(h_layout)

        h_splitter = Widgets.QSplitter(QtCore.Qt.Horizontal)
        h_splitter.addWidget(self.canvas)
        temp_widget = Widgets.QWidget()
        temp_widget.setLayout(v_layout)
        h_splitter.addWidget(temp_widget)
        temp_layout = Widgets.QVBoxLayout()
        temp_layout.addWidget(h_splitter)
        self.setLayout(temp_layout)

    def get_input(self):
        return (self.xs[int(self.left_x.value()) : int(self.right_x.value()) + 1],
                self.pixels[int(self.top_y.value()) : int(self.bottom_y.value()) + 1,
                            int(self.left_x.value()) : int(self.right_x.value()) + 1])

    def update_region_of_interest(self):
        self.top_line.remove()
        self.right_line.remove()
        self.bottom_line.remove()
        self.left_line.remove()
        self.top_line = self.axes.plot(*self.top_line_coordinates, color='r')[0]
        self.right_line = self.axes.plot(*self.right_line_coordinates, color='r')[0]
        self.bottom_line = self.axes.plot(*self.bottom_line_coordinates, color='r')[0]
        self.left_line = self.axes.plot(*self.left_line_coordinates, color='r')[0]
        self.canvas.draw()

    @property
    def top_line_coordinates(self):
        return (self.left_x.value(), self.right_x.value()), \
               (self.top_y.value(), self.top_y.value())

    @property
    def right_line_coordinates(self):
        return (self.right_x.value(), self.right_x.value()), \
               (self.top_y.value(), self.bottom_y.value())

    @property
    def bottom_line_coordinates(self):
        return (self.left_x.value(), self.right_x.value()), \
               (self.bottom_y.value(), self.bottom_y.value())

    @property
    def left_line_coordinates(self):
        return (self.left_x.value(), self.left_x.value()), \
               (self.top_y.value(), self.bottom_y.value())


class ListChoiceDialog(Widgets.QDialog):
    def __init__(self, choices, parent=None):
        super(ListChoiceDialog, self).__init__(parent)

        self.choices_group = Widgets.QButtonGroup()
        self.choices_group.setExclusive(True)

        self.choices_view = Widgets.QGroupBox()
        choices_view_layout = Widgets.QVBoxLayout()

        def add_choice(choice):
            choice_button = Widgets.QPushButton(choice)
            choice_button.setCheckable(True)
            self.choices_group.addButton(choice_button)
            h_layout = Widgets.QHBoxLayout()
            h_layout.addStretch(1)
            h_layout.addWidget(choice_button)
            h_layout.addStretch(1)
            choices_view_layout.addLayout(h_layout)

        for choice in choices:
            add_choice(choice)

        self.choices_view.setLayout(choices_view_layout)

        ok_button = Widgets.QPushButton('ok')
        ok_button.clicked.connect(self.accept)

        cancel_button = Widgets.QPushButton('cancel')
        cancel_button.clicked.connect(self.reject)

        button_layout = Widgets.QHBoxLayout()
        button_layout.addStretch(1)
        button_layout.addWidget(ok_button)
        button_layout.addWidget(cancel_button)

        v_layout = Widgets.QVBoxLayout()
        v_layout.addWidget(Widgets.QLabel('Please select your reference:'))
        v_layout.addWidget(self.choices_view)
        v_layout.addLayout(button_layout)
        self.setLayout(v_layout)

    def get_choice(self):
        return str(self.choices_group.checkedButton().text())


class CodeInjectionDialog(Widgets.QDialog):
    def __init__(self, parent=None):
        super(CodeInjectionDialog, self).__init__(parent)

        layout = Widgets.QVBoxLayout()

        layout.addWidget(Widgets.QLabel("Plot axes are available via 'axes'."))
        layout.addWidget(Widgets.QLabel("Figure is available via 'figure'."))
        layout.addWidget(Widgets.QLabel("Legend is available via 'legend'."))

        self.code_box = Widgets.QTextEdit()
        layout.addWidget(self.code_box)

        ok_button = Widgets.QPushButton('ok')
        ok_button.clicked.connect(self.accept)
        cancel_button = Widgets.QPushButton('cancel')
        cancel_button.clicked.connect(self.reject)
        button_layout = Widgets.QHBoxLayout()
        button_layout.addStretch(1)
        button_layout.addWidget(ok_button)
        button_layout.addWidget(cancel_button)

        layout.addLayout(button_layout)

        self.setLayout(layout)

    def get_commands(self):
        return str(self.code_box.toPlainText()).split('\n')
