# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import re
import xml.etree.ElementTree as XMLET


def prepare_data_file_gsi(filepath):
    with open(filepath) as fp:
        content = fp.read()

    if content[-1] != '\n':
        content += '\n'

    xml_version_pattern = '\<\?xml version="[\.0-9]+"\?\>'
    if not re.match(xml_version_pattern, content.split('\n')[0]):  # check if xml file has already valid header
        content = '<?xml version="1.0"?>\n' + '<Measurement>\n' + content + '</Measurement>\n'

    with open(filepath, str('w')) as fp:
        fp.write(str(content))
