# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, unicode_literals

import itertools

from matplotlib.figure import Figure
import matplotlib.patches as mpatches
import numpy as np
import png
import six

from ..tools.ipm_profiles import Profile1D
from . import pyqt45
from .dialogs import DropProfileDialog, ListChoiceDialog, RegionOfInterestDialog, \
    RegionOfInterestDialog2DInteractive, CodeInjectionDialog
from . import log

FigureCanvas = pyqt45.matplotlib_backend_qtXagg.FigureCanvasQTAgg
NavigationToolbar = pyqt45.matplotlib_backend_qtXagg.NavigationToolbar2QT

QtCore = pyqt45.QtCore
Widgets = pyqt45.Widgets

try:
    import scipy
except ImportError:
    scipy = None
else:
    from scipy.optimize import curve_fit
    from scipy.stats import chisquare

try:
    import ROOT
except ImportError:
    ROOT = None


def create_text_view_widget(text):
    widget = Widgets.QWidget()
    text_w = Widgets.QTextEdit()
    text_w.setReadOnly(True)
    text_w.setText(text)
    layout = Widgets.QVBoxLayout()
    layout.addWidget(text_w)
    widget.setLayout(layout)
    return widget


def accept_xs_and_ys(func):
    def wrapper(self, xs, ys, name=''):
        return func(self, Profile1D(xs, ys, name))

    return wrapper


def generate_color_cycle():
    return itertools.cycle(['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w'])


class BasicPlot(Widgets.QWidget):
    def __init__(self, parent=None):
        super(BasicPlot, self).__init__(parent)

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.show_custom_context_menu)

        action_clear = Widgets.QAction('Clear', self)
        action_clear.triggered.connect(self.clear_plots)

        self.custom_context_menu = Widgets.QMenu()
        self.custom_context_menu.addAction(action_clear)

    def show_custom_context_menu(self, pos):
        self.custom_context_menu.exec_(self.mapToGlobal(pos))


class StackPlot(BasicPlot):
    def __init__(self, parent=None):
        super(StackPlot, self).__init__(parent)

        log.logging.debug('StackPlot.__init__')

        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setParent(self)
        self.canvas.setSizePolicy(Widgets.QSizePolicy.Expanding, Widgets.QSizePolicy.Expanding)
        self.canvas.updateGeometry()
        # self.canvas.setMinimumSize(QtCore.QSize(200, 200))

        self.setup_axes()

        toolbar = NavigationToolbar(self.canvas, self)
        customize_plot_action = toolbar.addAction('Customize')
        customize_plot_action.triggered.connect(self.customize_plot)
        export_csv_plot_action = toolbar.addAction('Export CSV')
        export_csv_plot_action.triggered.connect(self.export_profiles_to_csv)

        layout = Widgets.QVBoxLayout()
        layout.addWidget(toolbar)
        layout.addWidget(self.canvas)
        self.setLayout(layout)

        self.setAcceptDrops(True)

        normalize_profile_to_one_action = Widgets.QAction('Normalize to 1', self)
        normalize_profile_to_one_action.triggered.connect(self.normalize_profiles_to_one)
        self.custom_context_menu.addAction(normalize_profile_to_one_action)

        bring_profiles_to_common_center_action = Widgets.QAction('Bring to common center', self)
        bring_profiles_to_common_center_action.triggered.connect(self.bring_profiles_to_common_center)
        self.custom_context_menu.addAction(bring_profiles_to_common_center_action)

        self.all_profile_menus = []

        self.remove_profile_menu = self.custom_context_menu.addMenu('Remove profile')
        self.remove_profile_menu.triggered.connect(self.remove_profile)
        self.all_profile_menus.append(self.remove_profile_menu)

        self.info_about_profile_menu = self.custom_context_menu.addMenu('Info about profile')
        self.info_about_profile_menu.triggered.connect(self.info_about_profile)
        self.all_profile_menus.append(self.info_about_profile_menu)

        if scipy is not None:
            self.fit_gaussian_available_profiles_menu = self.custom_context_menu.addMenu('Fit gaussian')
            self.fit_gaussian_available_profiles_menu.triggered.connect(self.fit_gaussian)
            self.all_profile_menus.append(self.fit_gaussian_available_profiles_menu)

        if ROOT is not None:
            self.fit_gaussian_root_available_profiles_menu = self.custom_context_menu.addMenu('Fit gaussian ROOT')
            self.fit_gaussian_root_available_profiles_menu.triggered.connect(self.fit_gaussian_root)
            self.all_profile_menus.append(self.fit_gaussian_root_available_profiles_menu)

        self.colors = generate_color_cycle()
        self.profiles = []
        self.handles = []
        self.labels = []
        self.plot_styles = {}
        self.legend = None

        self.info_views = []
        self.fit_result_views = []

        log.logging.debug('StackPlot.__init__ -> done')

    def setup_set_xml_view(self, xml_view):
        self.xml_view = xml_view

    def export_profiles_to_csv(self):
        filename = six.text_type(pyqt45.getSaveFileName(filter='CSV Files (*.csv)'))
        if not filename:
            return
        log.logging.debug('Exporting to %s' % filename)
        with open(filename, str('w')) as fp:
            for profile in self.profiles:
                fp.write(str('# %s\n' % profile.name))
                fp.write(str(
                    '\n'.join(map(
                        lambda x: '%s,%s' % (x[0], x[1]),
                        zip(profile.xs, profile.ys)
                    )) + '\n'
                ))

    def customize_plot(self):
        dialog = CodeInjectionDialog()
        if dialog.exec_():
            commands = dialog.get_commands()
            for command in commands:
                try:
                    exec('self.' + command)
                except (AttributeError, SyntaxError, TypeError, ValueError) as err:
                    Widgets.QMessageBox.critical(
                        self,
                        'Invalid command',
                        'Encountered the following error while executing "%s": %s' % (
                            command, six.text_type(err)
                        )
                    )
            self.canvas.draw()

    def dragEnterEvent(self, event):
        event.acceptProposedAction()

    def dropEvent(self, event):
        event.acceptProposedAction()
        tag = self.xml_view.get_current_tag()
        if tag == 'Profile':
            xs, ys = self.xml_view.get_current_profile()
            self.drop_1d(xs, ys)
        elif tag == 'Profile2D':
            self.drop_2d(self.xml_view.get_positions_of_current_profile2d())

    def drop_1d(self, xs, ys, can_be_fit=True):
        dialog = DropProfileDialog()
        if dialog.exec_():
            choice = dialog.get_choice()
            name = dialog.get_plot_name() or str(len(self.profiles) + 1)
            x_axis_scaling_factor = dialog.get_x_axis_scaling_factor()
            y_axis_scaling_factor = dialog.get_y_axis_scaling_factor()
        else:
            return

        if not len(xs) > 0:
            return

        xs = np.array(xs, dtype=float) * x_axis_scaling_factor
        ys = np.array(ys, dtype=float) * y_axis_scaling_factor

        if choice == 'add':
            self.add_profile(xs, ys)
        elif choice == 'subtract':
            self.add_profile(xs, -1 * ys)
        elif choice == 'stack':
            self.stack_profile(xs, ys, name, dialog.get_plot_style(), can_be_fit)

    def drop_2d(self, xs):
        dialog = RegionOfInterestDialog2DInteractive(self.xml_view.get_current_profile2d_filepath(), xs)
        if dialog.exec_():
            xs, pixels = dialog.get_input()
            xs = np.array(xs, dtype=float)
            pixels = np.array(pixels, dtype=float)
            ys = np.sum(pixels, axis=0) / pixels.shape[0]
            self.drop_1d(xs, ys)

    def add_profile(self, xs, ys, name=''):
        new_profile = Profile1D(xs, ys, name)
        for profile in self.profiles:
            profile.ys += new_profile.ys
        self.plot_profiles()

    def stack_profile(self, xs, ys, name, plot_style, can_be_fit):
        new_profile = Profile1D(xs, ys, str(name))

        self.remove_profile_menu.addAction(Widgets.QAction(new_profile.name, self))
        self.info_about_profile_menu.addAction(Widgets.QAction(new_profile.name, self))

        if can_be_fit:
            if scipy is not None:
                self.fit_gaussian_available_profiles_menu.addAction(Widgets.QAction(new_profile.name, self))
            if ROOT is not None:
                self.fit_gaussian_root_available_profiles_menu.addAction(Widgets.QAction(new_profile.name, self))

        self.profiles.append(new_profile)
        self.axes.plot(new_profile.xs, new_profile.ys, label=new_profile.name, **plot_style)
        self.handles.append(mpatches.Patch(color=plot_style['color'], label=new_profile.name))
        self.labels.append(new_profile.name)
        self.plot_styles[new_profile.name] = plot_style
        self.legend = self.figure.legend(handles=self.handles, labels=self.labels)
        self.canvas.draw()

    def remove_profile(self, action):
        profile = next(filter(lambda x: x.name == action.text(), self.profiles))
        self.profiles.remove(profile)
        self.plot_styles.pop(profile.name)
        self.plot_profiles()

        def remove_action_from_menu(menu):
            try:
                menu.removeAction(next(a for a in menu.actions() if str(a.text()) == profile.name))
            except AttributeError:
                pass

        for menu in self.all_profile_menus:
            remove_action_from_menu(menu)

    def info_about_profile(self, action):
        profile = next(filter(lambda x: x.name == action.text(), self.profiles))
        self.info_views.append(ProfileInfoView(profile))
        self.info_views[-1].show()

    def normalize_profiles_to_one(self):
        for profile in self.profiles:
            profile.ys = profile.ys / max(profile.ys)
        self.plot_profiles()

    def bring_profiles_to_common_center(self):
        dialog = ListChoiceDialog([p.name for p in self.profiles])
        if dialog.exec_():
            ref_profile_name = dialog.get_choice()
        else:
            return

        def get_peak_x(profile):
            return max(zip(profile.xs, profile.ys), key=lambda p: p[1])[0]

        ref_profile = next(p for p in self.profiles if p.name == ref_profile_name)
        ref_peak_x = get_peak_x(ref_profile)
        for profile in self.profiles:
            peak_distance = get_peak_x(profile) - ref_peak_x
            profile.xs = np.array(profile.xs) - peak_distance
        self.plot_profiles()

    def fit_gaussian(self, action):
        def gauss(x, peak, center, sigma, offset):
            return peak * np.exp(- (x - center) ** 2 / (2 * sigma ** 2)) + offset

        def expectation_value(xs, ys):
            return sum(xs * ys / sum(ys))

        def standard_deviation(xs, ys):
            return np.sqrt(expectation_value((xs - expectation_value(xs, ys)) ** 2, ys))

        def region_of_interest(xs, ys, xmin, xmax):
            xs, ys = list(zip(*filter(lambda x: x[0] >= xmin and x[0] <= xmax, zip(xs, ys))))
            return np.array(xs), np.array(ys)

        region_of_interest_dialog = RegionOfInterestDialog()
        if region_of_interest_dialog.exec_():
            xmin, xmax = region_of_interest_dialog.get_xmin_xmax()
        else:
            return

        profile = next(filter(lambda x: x.name == action.text(), self.profiles)[0])
        xs, ys = region_of_interest(profile.xs, profile.ys, xmin, xmax)

        popt, pcov = curve_fit(
            gauss, xs, ys,
            p0=(
                max(ys),
                expectation_value(xs, ys),
                standard_deviation(xs, ys),
                (ys[0] + ys[-1]) / 2.
            )
        )

        errors = np.sqrt(np.diag(pcov))
        popt_with_errors = np.array(list(zip(popt, errors)))

        fit_ys = gauss(xs, *popt)

        self.drop_1d(xs, fit_ys, can_be_fit=False)

        output = ''
        output += '--------------------------------------------------------------------------------------------------\n'
        output += '               Fit Results (scipy)                \n'
        output += '              ---------------------               \n'
        output += '\n'
        output += 'fit parameters:\n'
        output += str(np.array(['peak', 'center', 'sigma', 'offset'])) + '\n'
        output += str(popt) + '\n'
        output += str(popt_with_errors) + '\n'
        output += '\n'
        output += 'covariance matrix:\n'
        output += str(pcov) + '\n'
        output += '\n'
        output += 'chi squared:\n'
        output += str(chisquare(ys, fit_ys)[0] / len(ys)) + '\n'
        output += '--------------------------------------------------------------------------------------------------\n'

        print(output)
        self.fit_result_views.append(create_text_view_widget(output))
        self.fit_result_views[-1].setWindowTitle(profile.name)
        self.fit_result_views[-1].show()

    def fit_gaussian_root(self, action):
        def expectation_value(xs, ys):
            return sum(xs * ys / sum(ys))

        def standard_deviation(xs, ys):
            return np.sqrt(expectation_value((xs - expectation_value(xs, ys)) ** 2, ys))

        def region_of_interest(xs, ys, xmin, xmax):
            xs, ys = list(zip(*filter(lambda x: x[0] >= xmin and x[0] <= xmax, zip(xs, ys))))
            return np.array(xs), np.array(ys)

        region_of_interest_dialog = RegionOfInterestDialog()
        if region_of_interest_dialog.exec_():
            xmin, xmax = region_of_interest_dialog.get_xmin_xmax()
        else:
            return

        profile = next(filter(lambda x: x.name == action.text(), self.profiles))
        xs, ys = region_of_interest(profile.xs, profile.ys, xmin, xmax)
        dx = xs[1] - xs[0]

        histogram = ROOT.TH1D(
            'fit ' + profile.name, 'fit ' + profile.name, len(xs), np.insert(xs - dx / 2, len(xs), xs[-1] + dx / 2))

        histogram.FillN(len(xs), xs, ys)

        fit_function = ROOT.TF1(
            'gaussian fit',
            '[0] * exp( - (x - [1]) * (x - [1]) / (2.0 * [2] * [2]) ) + [3]',
            xs[0] - dx / 2, xs[-1] + dx / 2)
        fit_function.SetParameter(0, max(ys))
        fit_function.SetParameter(1, expectation_value(xs, ys))
        fit_function.SetParameter(2, standard_deviation(xs, ys))
        fit_function.SetParameter(3, (ys[0] + ys[-1]) / 2)

        fit_result = histogram.Fit(fit_function, 'MR0S')

        fit_ys = np.array([fit_function.Eval(x) for x in xs])

        self.drop_1d(xs, fit_ys, can_be_fit=False)

        covariance_matrix = fit_result.GetCovarianceMatrix()

        output = ''
        output += '\n'
        output += '--------------------------------------------------------------------------------------------------\n'
        output += '               Fit Results (ROOT)                 \n'
        output += '              --------------------                \n'
        output += '\n'
        output += 'fit parameters:\n'
        output += str(np.array(['peak', 'center', 'sigma', 'offset'])) + '\n'
        output += str(np.array([fit_function.GetParameter(0), fit_function.GetParameter(1),
                                fit_function.GetParameter(2), fit_function.GetParameter(3)])) + '\n'
        output += str(np.array([[fit_result.Parameter(i), fit_result.ParError(i)] for i in range(4)])) + '\n'
        # output += '(%e +/- %e, %e +/- %e, %e +/- %e, %e +/- %e)\n' % (
        #     fit_result.Parameter(0), fit_result.ParError(0), fit_result.Parameter(1), fit_result.ParError(1),
        #     fit_result.Parameter(2), fit_result.ParError(2), fit_result.Parameter(3), fit_result.ParError(3))
        output += '\n'
        output += 'covariance matrix:\n'
        output += str(np.array([[covariance_matrix(i, j) for j in range(4)] for i in range(4)])) + '\n'
        output += '\n'
        output += 'chi squared:\n'
        output += str(fit_result.Chi2()) + '\n'
        output += '--------------------------------------------------------------------------------------------------\n'
        output += '\n'

        print(output)
        self.fit_result_views.append(create_text_view_widget(output))
        self.fit_result_views[-1].setWindowTitle(profile.name)
        self.fit_result_views[-1].show()

    def clear_canvas(self):
        self.figure.clear()
        self.axes = self.figure.add_subplot(111)
        self.canvas.draw()

    def clear_plots(self):
        self.profiles = []
        self.handles = []
        self.labels = []
        self.plot_styles = {}
        self.clear_canvas()

        def clear_menu(menu):
            for action in menu.actions():
                menu.removeAction(action)

        for menu in self.all_profile_menus:
            clear_menu(menu)

    def plot_profiles(self):
        self.colors = generate_color_cycle()
        self.handles = []
        self.labels = []
        self.clear_canvas()
        for profile in self.profiles:
            self.axes.plot(profile.xs, profile.ys, label=profile.name, **self.plot_styles[profile.name])
            self.handles.append(mpatches.Patch(color=self.plot_styles[profile.name]['color'], label=profile.name))
            self.labels.append(profile.name)
        self.legend = self.figure.legend(handles=self.handles, labels=self.labels)
        self.canvas.draw()

    def setup_axes(self):
        self.axes.set_xlabel('x [mm]')
        self.axes.set_ylabel('signal')


class MiniPlot(BasicPlot):
    def __init__(self, stack_plot, parent=None):
        super(MiniPlot, self).__init__(parent)

        log.logging.debug('MiniPlot.__init__')

        self.stack_plot = stack_plot

        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setParent(self)
        self.canvas.setSizePolicy(Widgets.QSizePolicy.Expanding, Widgets.QSizePolicy.Expanding)
        self.canvas.updateGeometry()
        # self.canvas.setMinimumSize(QtCore.QSize(200, 200))

        self.setup_axes()

        layout = Widgets.QVBoxLayout()
        layout.addWidget(self.canvas)
        self.setLayout(layout)

        action_stack = Widgets.QAction('Add to stacked plot', self)
        action_stack.triggered.connect(self.add_to_stacked_plot)
        self.custom_context_menu.addAction(action_stack)

        log.logging.debug('MiniPlot.__init__ -> done')

    def plot(self, xs, ys):
        self.xs, self.ys = xs, ys
        self.clear_canvas()
        self.axes.plot(xs, ys)
        self.setup_axes()
        self.canvas.draw()

    def add_to_stacked_plot(self):
        self.stack_plot.drop_1d(self.xs, self.ys)

    def clear_canvas(self):
        self.figure.clear()
        self.axes = self.figure.add_subplot(111)
        self.canvas.draw()

    def clear_plots(self):
        self.clear_canvas()

    def setup_axes(self):
        self.axes.set_xlabel('x [mm]')
        self.axes.set_ylabel('amplitude [a.u.]')


class MiniPlot2D(Widgets.QWidget):
    def __init__(self, parent=None):
        super(MiniPlot2D, self).__init__(parent)

        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setParent(self)
        self.canvas.setSizePolicy(Widgets.QSizePolicy.Expanding, Widgets.QSizePolicy.Expanding)
        self.canvas.updateGeometry()
        # self.canvas.setMinimumSize(QtCore.QSize(200, 200))

        self.setup_axes()

        layout = Widgets.QVBoxLayout()
        layout.addWidget(self.canvas)
        self.setLayout(layout)

    def plot(self, xs, ys, zs):
        self.clear_canvas()
        plot = self.axes.pcolor(xs, ys, zs, cmap='gray')
        cbar = self.figure.colorbar(plot)
        cbar.ax.set_ylabel('amplitude [a.u.]')
        self.setup_axes()
        self.canvas.draw()

    def plot_png(self, zs):
        self.clear_canvas()
        self.axes.imshow(zs, cmap='gray')
        self.setup_axes()
        self.canvas.draw()

    def clear_canvas(self):
        self.figure.clear()
        self.axes = self.figure.add_subplot(111)
        self.canvas.draw()

    def setup_axes(self):
        self.axes.set_xlabel('x [mm]')
        self.axes.set_ylabel('z [a.u.]')


class PlotPreviews(Widgets.QStackedWidget):
    def __init__(self, stack_plot, parent=None):
        super(PlotPreviews, self).__init__(parent)

        self.preview_1d = MiniPlot(stack_plot)
        self.preview_2d = MiniPlot2D()

        self.addWidget(self.preview_1d)
        self.addWidget(self.preview_2d)

        self.setCurrentWidget(self.preview_1d)

    def plot_1d(self, xs, ys):
        self.setCurrentWidget(self.preview_1d)
        self.preview_1d.plot(xs, ys)

    def plot_2d(self, xs, ys, zs):
        self.setCurrentWidget(self.preview_2d)
        self.preview_2d.plot(xs, ys, zs)

    def plot_png(self, filename):
        self.setCurrentWidget(self.preview_2d)
        image = png.Reader(filename)
        width, height, pixels, meta = image.read()
        pixels2d = np.asarray(list(pixels))
        self.preview_2d.plot_png(pixels2d)


class ProfileInfoView(Widgets.QWidget):
    def __init__(self, profile, parent=None):
        super(ProfileInfoView, self).__init__(parent)

        self.plot = MiniPlot()
        self.quantities = Widgets.QTableWidget()

        h_splitter = Widgets.QSplitter(QtCore.Qt.Horizontal)
        h_splitter.addWidget(self.plot)
        h_splitter.addWidget(self.quantities)

        h_layout = Widgets.QHBoxLayout()
        h_layout.addWidget(h_splitter)
        self.setLayout(h_layout)

        self.plot.plot(profile.xs, profile.ys)

        self.quantities.setColumnCount(2)

        def add_quantity(name, value):
            self.quantities.insertRow(self.quantities.rowCount())
            table_item_name = Widgets.QTableWidgetItem(name)
            self.quantities.setItem(self.quantities.rowCount() - 1, 0, table_item_name)
            table_item_value = Widgets.QTableWidgetItem(str(value))
            self.quantities.setItem(self.quantities.rowCount() - 1, 1, table_item_value)

        add_quantity('Peak position', profile.peak_position)
        add_quantity('Mean value', profile.mean)
        add_quantity('Standard deviation', profile.standard_deviation)
        add_quantity('Kurtosis', profile.kurtosis)
        add_quantity('Excess kurtosis', profile.excess_kurtosis)
        add_quantity('Integral', profile.integral)
