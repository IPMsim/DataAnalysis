# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import os

import six

from . import pyqt45
from . import log
from .csv_view import CSVView
from .plotview import PlotPreviews, StackPlot
from .xmlview import XMLView

QtCore = pyqt45.QtCore
Widgets = pyqt45.Widgets

__version__ = '0.0.1'


class MainWidget(Widgets.QWidget):
    def __init__(self, parent):
        super(MainWidget, self).__init__(parent)

        log.logging.debug('MainWidget.__init__')

        self.plotview = StackPlot()
        self.miniplot = PlotPreviews(self.plotview)
        v_layout = Widgets.QVBoxLayout()
        v_layout.addWidget(Widgets.QLabel('Plot preview:'))
        v_layout.addWidget(self.miniplot)
        miniplot_container = Widgets.QWidget()
        miniplot_container.setLayout(v_layout)

        self.xmlview = XMLView(self.miniplot)
        self.csvview = CSVView(self.miniplot)
        self.plotview.setup_set_xml_view(self.xmlview)

        self.views = Widgets.QStackedWidget()
        self.views.addWidget(self.xmlview)
        self.views.addWidget(self.csvview)

        views_layout = Widgets.QVBoxLayout()
        open_file_button = Widgets.QPushButton('Open file')
        open_file_button.clicked.connect(parent.open_file)
        open_file_layout = Widgets.QHBoxLayout()
        open_file_layout.addWidget(open_file_button)
        open_file_layout.addStretch(1)
        views_layout.addLayout(open_file_layout)
        views_layout.addWidget(self.views)
        views_container = Widgets.QWidget()
        views_container.setLayout(views_layout)

        v_splitter = Widgets.QSplitter()
        v_splitter.setOrientation(QtCore.Qt.Vertical)
        v_splitter.addWidget(self.plotview)
        v_splitter.addWidget(miniplot_container)

        h_splitter = Widgets.QSplitter()
        h_splitter.setOrientation(QtCore.Qt.Horizontal)
        h_splitter.addWidget(views_container)
        h_splitter.addWidget(v_splitter)

        layout = Widgets.QVBoxLayout()
        layout.addWidget(h_splitter)
        self.setLayout(layout)

        log.logging.debug('MainWidget.__init__ -> done')

    def change_view(self, kind):
        if kind == 'csv':
            self.views.setCurrentWidget(self.csvview)
        elif kind == 'xml':
            self.views.setCurrentWidget(self.xmlview)


class MainWindow(Widgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        log.logging.debug('MainWindow.__init__')

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        # self.xml_view = XMLView()
        # self.setCentralWidget(self.xml_view)

        self.mainwidget = MainWidget(self)
        self.setCentralWidget(self.mainwidget)

        menubar = self.menuBar()

        file_menu = menubar.addMenu('File')
        open_file_action = Widgets.QAction('Open file', self.mainwidget.xmlview)
        open_file_action.triggered.connect(self.open_file)
        file_menu.addAction(open_file_action)
        file_menu.addSeparator()
        export_csv_action = Widgets.QAction('Export profiles to CSV', self.mainwidget.plotview)
        export_csv_action.triggered.connect(self.mainwidget.plotview.export_profiles_to_csv)
        file_menu.addAction(export_csv_action)

        help_menu = menubar.addMenu('Help')
        show_about_action = Widgets.QAction('About', self)
        show_about_action.triggered.connect(self.show_about)
        help_menu.addAction(show_about_action)

        read_me_action = Widgets.QAction('Read me', self)
        read_me_action.triggered.connect(self.readme)
        help_menu.addAction(read_me_action)

        self.setWindowTitle('[IPMSim] Data Analysis GUI')

        log.logging.debug('MainWindow.__init__ -> done')

    def open_file(self):
        filepath = six.text_type(pyqt45.getOpenFileName(
            filter='Data Files (*.csv *.csv.gz *.txt *.txt.gz *.xml)'))
        suffix = os.path.splitext(filepath)[-1]
        if suffix in ('.csv', '.txt'):
            self.mainwidget.change_view('csv')
            self.mainwidget.csvview.open_file(filepath)
        elif suffix in ('.xml',):
            self.mainwidget.change_view('xml')
            self.mainwidget.xmlview.open_file(filepath)

    def show_about(self):
        info = '%s\nVersion %s' % (self.windowTitle(), __version__)
        Widgets.QMessageBox.information(self, 'About', info)

    def readme(self):
        f = lambda x: os.path.split(x)[0]
        root_dir = f(f(f(__file__)))
        with open(os.path.join(root_dir, 'README.rst')) as fp:
            readme_text = fp.read()
        Widgets.QMessageBox.information(self, 'Read me', readme_text)
