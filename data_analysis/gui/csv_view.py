# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import numpy

from . import pyqt45

QtGui = pyqt45.QtGui
Widgets = pyqt45.Widgets


class CSVView(Widgets.QWidget):
    def __init__(self, miniplot, parent=None):
        super(CSVView, self).__init__(parent)

        self.miniplot = miniplot

        self.preview = Widgets.QTextEdit()
        self.preview.setReadOnly(True)
        self.preview.setWordWrapMode(QtGui.QTextOption.NoWrap)

        layout = Widgets.QVBoxLayout()
        layout.addWidget(Widgets.QLabel('Preview:'))
        layout.addWidget(self.preview)
        self.setLayout(layout)

    def open_file(self, filepath):
        with open(filepath) as fp:
            self.preview.setText(fp.read())

        data = numpy.loadtxt(filepath)

        xs = data[:, 0]
        ys = data[:, 1]

        self.miniplot.plot_1d(xs, ys)
