# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, unicode_literals

import numpy as np

try:
    import scipy
except ImportError:
    scipy = None
else:
    import scipy.optimize
    import scipy.stats

try:
    import ROOT
except ImportError:
    ROOT = None


class Profile1D:
    def __init__(self, xs, ys, name=''):
        if len(xs) != len(ys):
            raise UserWarning('x-values and y-values must have same length')

        self.xs = np.array(xs)
        self.ys = np.array(ys)
        self.name = name

        # TODO: multiple peak indices possible -> warn user or generate average?
        self._peak_index = self.ys.tolist().index(np.max(ys))

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    @property
    def peak_position(self):
        return self.xs[self._peak_index]

    @property
    def integral(self):
        return np.sum(self.ys)

    @property
    def mean(self):
        return Profile1D.expectation_value(self.xs, self.ys)

    @property
    def standard_deviation(self):
        exp_value = Profile1D.expectation_value
        return np.sqrt(exp_value((self.xs - exp_value(self.xs, self.ys))**2, self.ys))

    @property
    def kurtosis(self):
        exp_value = Profile1D.expectation_value
        return exp_value((self.xs - exp_value(self.xs, self.ys))**4, self.ys) / self.standard_deviation ** 4

    @property
    def excess_kurtosis(self):
        return self.kurtosis - 3

    @staticmethod
    def expectation_value(xs, ys):
        return np.sum(xs * ys / np.sum(ys))

    def bring_to_zero(self):
        self.ys -= np.min(self.ys)

    def boundaries_for_integral(self, integral_ratio):
        if integral_ratio > 1.0:
            raise ValueError('Ratio must be in [0, 1]')
        integral_in = integral_ratio * self.integral             # integral enclosed by boundaries
        running_integral = self.ys[self._peak_index]
        running_index = 1
        while running_integral < integral_in:
            if self._peak_index - running_index >= 0:
                running_integral += self.ys[self._peak_index - running_index]
            if self._peak_index + running_index < len(self.ys):
                running_integral += self.ys[self._peak_index + running_index]
            running_index += 1

        running_index -= 1

        return self.xs[self._peak_index - running_index], self.xs[self._peak_index + running_index]

    def boundaries_for_height(self, peak_ratio):
        if peak_ratio > 1.0:
            raise ValueError('Ratio must be in [0, 1]')

        def run_index(index, threshold, increment):
            while 0 <= index < len(self.ys) and self.ys[index] >= threshold:
                index += increment
            return index - increment

        y_threshold = peak_ratio * self.ys[self._peak_index]

        return self.xs[run_index(self._peak_index, y_threshold, -1)],\
               self.xs[run_index(self._peak_index, y_threshold, 1)]

    def boundaries_for_standard_deviation(self, n_std_dev):
        std_dev = self.standard_deviation
        return self.xs[self._peak_index] - n_std_dev * std_dev, self.xs[self._peak_index] + n_std_dev * std_dev

    def has_sufficient_height(self, multiplier=None, absolute_difference=None, relative_difference_ratio=None):
        if len(filter(None, [multiplier, absolute_difference, relative_difference_ratio])) != 1:
            raise ValueError('Specify either multiplier, absolute difference or relative difference ratio')

        peak_value = self.ys[self._peak_index]
        if multiplier:
            return peak_value / self.ys[0] >= multiplier and peak_value / self.ys[-1] >= multiplier

        if absolute_difference:
            return peak_value - self.ys[0] >= absolute_difference and peak_value - self.ys[-1] >= absolute_difference

        if relative_difference_ratio:
            return peak_value / self.ys[0] - 1.0 >= relative_difference_ratio and \
                   peak_value / self.ys[-1] - 1.0 >= relative_difference_ratio

    def create_copy_between_boundaries(self, xmin, xmax):
        xs, ys = zip(*filter(lambda x: xmin <= x[0] <= xmax, zip(self.xs, self.ys)))
        return Profile1D(xs, ys, self.name + '_from_%.1f_to_%.1f' % (xmin, xmax))

    if scipy is not None:
        def fit_gaussian_with_offset_scipy(self, x_min_max=None, log_to_stdout=False):
            """
            Perform fit using scipy library
            :param x_min_max: defines region of interest
            :return: sigma, mean
            """

            if x_min_max is None:
                region_of_interest = self
                xs, ys = self.xs, self.ys
            elif isinstance(x_min_max, tuple):
                xmin, xmax = x_min_max[0], x_min_max[1]
                region_of_interest = self.create_copy_between_boundaries(xmin, xmax)
                xs, ys = region_of_interest.xs, region_of_interest.ys
            else:
                raise TypeError('x_min_max must be None or tuple')

            def gauss(x, peak, center, sigma, offset):
                return peak * np.exp(- (x - center) ** 2 / (2 * sigma ** 2)) + offset

            popt, pcov = scipy.optimize.curve_fit(gauss, xs, ys,
                                                  p0=(max(ys),
                                                      region_of_interest.mean,
                                                      region_of_interest.standard_deviation,
                                                      (ys[0] + ys[-1]) / 2.))

            errors = np.sqrt(np.diag(pcov))
            popt_with_errors = np.array(zip(popt, errors))

            fit_ys = gauss(xs, *popt)

            chi2 = scipy.stats.chisquare(ys, fit_ys)[0] / len(ys)

            if log_to_stdout:
                print('----------------------------------------------------------------------------------------------------')
                print('               Fit Results (scipy)                ')
                print('              ---------------------               ')
                print('')
                print('fit parameters:')
                print(np.array(['peak', 'center', 'sigma', 'offset']))
                print(popt)
                print(popt_with_errors)
                print('')
                print('covariance matrix:')
                print(pcov)
                print('')
                print('chi squared:')
                print(chi2)
                print('----------------------------------------------------------------------------------------------------')

            return chi2, popt[2], popt[1], xs, fit_ys

        def fit_gaussian_plus_polynomial_scipy(self, degree=0, x_min_max=None, log_to_stdout=False):
            """
            Perform fit using scipy library
            :param x_min_max: defines region of interest
            :return: sigma, mean
            """

            if x_min_max is None:
                region_of_interest = self
                xs, ys = self.xs, self.ys
            elif isinstance(x_min_max, tuple):
                xmin, xmax = x_min_max[0], x_min_max[1]
                region_of_interest = self.create_copy_between_boundaries(xmin, xmax)
                xs, ys = region_of_interest.xs, region_of_interest.ys
            else:
                raise TypeError('x_min_max must be None or tuple')

            def gauss(x, peak, center, sigma, *args):
                return peak * np.exp(- (x - center) ** 2 / (2 * sigma ** 2)) + np.poly1d(list(reversed(args)))(x)

            p0 = tuple([max(ys), region_of_interest.mean, region_of_interest.standard_deviation, (ys[0] + ys[-1]) / 2.]
                       + [0.0 for _ in range(degree)])

            popt, pcov = scipy.optimize.curve_fit(gauss, xs, ys, p0=p0)

            if not (np.all(np.isfinite(popt)) and np.all(np.isfinite(pcov))):
                raise ValueError('Non-finite encountered')

            errors = np.sqrt(np.diag(pcov))
            popt_with_errors = np.array(zip(popt, errors))

            fit_ys = gauss(xs, *popt)

            chi2 = scipy.stats.chisquare(ys, fit_ys)[0] / len(ys)

            if log_to_stdout:
                print('----------------------------------------------------------------------------------------------------')
                print('               Fit Results (scipy)                ')
                print('              ---------------------               ')
                print('')
                print('fit parameters:')
                print(np.array(['peak', 'center', 'sigma', 'offset']))
                print(popt)
                print(popt_with_errors)
                print('')
                print('covariance matrix:')
                print(pcov)
                print('')
                print('chi squared:')
                print(chi2)
                print('----------------------------------------------------------------------------------------------------')

            return chi2, popt[2], popt[1], xs, fit_ys

    if ROOT is not None:
        def fit_gaussian_with_offset_root(self, x_min_max=None, log_to_stdout=False):
            """
            Perform fit using ROOT library
            :param x_min_max: defines region of interest
            :return: sigma, mean
            """

            if x_min_max is None:
                region_of_interest = self
                xs, ys = self.xs, self.ys
            elif isinstance(x_min_max, tuple):
                xmin, xmax = x_min_max[0], x_min_max[1]
                region_of_interest = self.create_copy_between_boundaries(xmin, xmax)
                xs, ys = region_of_interest.xs, region_of_interest.ys
            else:
                raise TypeError('x_min_max must be None or tuple')

            dx = xs[1] - xs[0]

            histogram = ROOT.TH1D(
                'fit ' + self.name, 'fit ' + self.name, len(xs), np.insert(xs - dx / 2, len(xs), xs[-1] + dx / 2))

            histogram.FillN(len(xs), xs, ys)

            fit_function = ROOT.TF1(
                'gaussian fit',
                '[0] * exp( - (x - [1]) * (x - [1]) / (2.0 * [2] * [2]) ) + [3]',
                xs[0] - dx / 2, xs[-1] + dx / 2)
            fit_function.SetParameter(0, max(ys))
            fit_function.SetParameter(1, region_of_interest.mean)
            fit_function.SetParameter(2, region_of_interest.standard_deviation)
            fit_function.SetParameter(3, (ys[0] + ys[-1]) / 2)

            fit_options = 'MR0S'
            if not log_to_stdout:
                fit_options += 'Q'
            fit_result = histogram.Fit(fit_function, fit_options)

            fit_ys = np.array([fit_function.Eval(x) for x in xs])

            covariance_matrix = fit_result.GetCovarianceMatrix()

            if log_to_stdout:
                print('')
                print('----------------------------------------------------------------------------------------------------')
                print('               Fit Results (ROOT)                 ')
                print('              --------------------                ')
                print('')
                print('fit parameters:')
                print(np.array(['peak', 'center', 'sigma', 'offset']))
                print(np.array([fit_function.GetParameter(0), fit_function.GetParameter(1), fit_function.GetParameter(2),
                                fit_function.GetParameter(3)]))
                print(np.array([[fit_result.Parameter(i), fit_result.ParError(i)] for i in range(4)]))
                # print('(%e +/- %e, %e +/- %e, %e +/- %e, %e +/- %e)' % (
                #     fit_result.Parameter(0), fit_result.ParError(0), fit_result.Parameter(1), fit_result.ParError(1),
                #     fit_result.Parameter(2), fit_result.ParError(2), fit_result.Parameter(3), fit_result.ParError(3)))
                print('')
                print('covariance matrix:')
                print(np.array([[covariance_matrix(i, j) for j in range(4)] for i in range(4)]))
                print('')
                print('chi squared:')
                print(fit_result.Chi2())
                print('----------------------------------------------------------------------------------------------------')
                print('')

            return fit_result.Chi2(), fit_function.GetParameter(2), fit_function.GetParameter(1), xs, fit_ys
