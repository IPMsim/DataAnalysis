from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import png
from PyQt4 import QtCore, QtGui


class MiniPlot2D(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MiniPlot2D, self).__init__(parent)

        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setParent(self)
        self.canvas.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.canvas.updateGeometry()
        # self.canvas.setMinimumSize(QtCore.QSize(200, 200))

        self.setup_axes()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.canvas)
        self.setLayout(layout)

    def plot(self, filepath):
        image = png.Reader(str(filepath))
        width, height, pixels, meta = image.read()
        pixels2d = np.asarray(list(pixels))
        self.clear_canvas()
        self.axes.imshow(pixels2d, cmap='gray')
        self.setup_axes()
        self.canvas.draw()

    def clear_canvas(self):
        self.figure.clear()
        self.axes = self.figure.add_subplot(111)
        self.canvas.draw()

    def setup_axes(self):
        self.axes.set_xlabel('x [mm]')
        self.axes.set_ylabel('z [a.u.]')
