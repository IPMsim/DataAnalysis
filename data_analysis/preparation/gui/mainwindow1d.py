import datetime
from fileviewer1d import InteractiveFileBrowser
import numpy as np
import os.path
from PyQt4 import QtGui, QtCore
import time


class MainWindow1D(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MainWindow1D, self).__init__()

        self.fileview = InteractiveFileBrowser()
        self.xml_text_h = QtGui.QTextEdit()
        self.xml_text_h.setReadOnly(True)
        self.xml_text_v = QtGui.QTextEdit()
        self.xml_text_v.setReadOnly(True)

        def widget_with_title(widget, title):
            v_layout = QtGui.QVBoxLayout()
            v_layout.addWidget(QtGui.QLabel(title))
            v_layout.addWidget(widget)
            container = QtGui.QWidget()
            container.setLayout(v_layout)
            return container

        h_splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        h_splitter.addWidget(self.fileview)
        h_splitter.addWidget(widget_with_title(self.xml_text_h, 'horizontal'))
        h_splitter.addWidget(widget_with_title(self.xml_text_v, 'vertical'))

        h_layout = QtGui.QHBoxLayout()
        h_layout.addWidget(h_splitter)
        self.setLayout(h_layout)

        self.fileview.selectionModel().currentChanged.connect(self.file_selected)

    def file_selected(self, index, _):
        xml_h, xml_v = self.create_xml_text(self.fileview.model.filePath(index))
        self.xml_text_h.setText(xml_h)
        self.xml_text_v.setText(xml_v)

    @staticmethod
    def create_xml_text(filepath):
        name = os.path.splitext(os.path.split(str(filepath))[-1])[0]
        date_s, time_s = name.split('_')
        datetime_string = '20%s-%s-%s %s:%s:%s' % (date_s[:2], date_s[2:4], date_s[4:6],
                                                   time_s[:2], time_s[2:4], time_s[4:6])
        print 'Datetime: %s' % datetime_string
        datetime_format = "%Y-%m-%d %H:%M:%S"
        unix_time = (time.mktime(datetime.datetime.strptime(datetime_string, datetime_format).timetuple()))
        print '%.3f' % unix_time

        profile_time_distance = 10  # [ms]

        n_wires = 64
        nrs, ys_h, ys_v = MainWindow1D.get_columns_from_file(filepath, delim='  ', skip_start=1, cast_to=str)
        assert len(ys_h) % n_wires == 0, 'Number of data points is no multiple of %s' % n_wires
        ys_h, ys_v = (np.array(ys_h).reshape(len(ys_h) / n_wires, n_wires),
                      np.array(ys_v).reshape(len(ys_v) / n_wires, n_wires))

        def write_xml(ys):
            profile_elements = []
            i = 1
            for y in ys:
                unix_time_str = '%.3f' % (unix_time + ((i-1)*profile_time_distance) / 1000.)
                timestamp_tag = MainWindow1D.write_tag_and_text('Timestamp', unix_time_str, {'unit': '"s"'})
                position_tag = MainWindow1D.write_tag_and_text('Position', '', {'unit': '"mm"'})
                amplitude_tag = MainWindow1D.write_tag_and_text('Amplitude', ','.join(y), {'unit': '"arbitrary"'})
                profile_tag = MainWindow1D.write_tag_and_text_indentation('Profile',
                                                                        '\n'.join([timestamp_tag, position_tag,
                                                                                   amplitude_tag]),
                                                                        {'id': '"%d"' % i, 'type': '"measured"'})
                profile_elements.append(profile_tag)
                i += 1
            profile_elements.append(MainWindow1D.write_tag_and_text('Comment', '', {}))
            profileset_tag = MainWindow1D.write_tag_and_text_indentation('ProfileSet', '\n'.join(profile_elements), {})
            return profileset_tag

        return write_xml(ys_h), write_xml(ys_v)

    @staticmethod
    def write_tag_and_text(tag, text, attributes, level=0):
        indent = level * '    '
        attribute_string = MainWindow1D.get_attribute_string(attributes)
        return indent + ('<%s%s>%s</%s>' % (tag, attribute_string, text, tag))

    @staticmethod
    def write_tag_and_text_indentation(tag, text, attributes, level=0):
        indent = level * '    '
        more_indent = indent + '    '
        attribute_string = MainWindow1D.get_attribute_string(attributes)
        return '%s<%s%s>\n%s\n%s</%s>' % (indent, tag, attribute_string,
                                          '\n'.join([more_indent + l for l in text.split('\n')]), indent, tag)

    @staticmethod
    def get_attribute_string(attributes):
        attribute_string = ' '.join(['%s=%s' % (k, v) for k, v in iter(attributes.items())])
        if attribute_string:
            attribute_string = ' ' + attribute_string
        return attribute_string

    @staticmethod
    def get_rows_from_file(filepath, delim=' ', skip='#', skip_start=0, cast_to=str):
        with open(filepath) as fp:
            lines = fp.read().strip().split('\n')
            return [[cast_to(e) for e in l.split(delim)] for l in lines[skip_start:]
                    if len(l) > 0 and not l.startswith(skip)]

    @staticmethod
    def get_columns_from_file(*args, **kwargs):
        return zip(*MainWindow1D.get_rows_from_file(*args, **kwargs))
