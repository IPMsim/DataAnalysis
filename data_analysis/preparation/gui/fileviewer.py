import os.path
from PyQt4 import QtGui, QtCore


class FileView(QtGui.QTreeView):
    file_selected = QtCore.pyqtSignal('QString')

    def __init__(self, selectionsview_1, selectionsview, plotview, parent=None):
        super(FileView, self).__init__(parent)

        self.selectionsview_1 = selectionsview_1
        self.selectionsview = selectionsview
        self.plotview = plotview

        root_dir = '/'

        self.model = QtGui.QFileSystemModel()
        self.model.setRootPath(root_dir)
        self.model.setNameFilters(['*.png'])
        self.model.setNameFilterDisables(False)
        self.setModel(self.model)

        self.setRootIndex(self.model.index(root_dir))

        self.setSelectionModel(QtGui.QItemSelectionModel(self.model))
        self.selectionModel().currentChanged.connect(self.file_changed)

    def file_changed(self, index, _):
        self.plotview.plot(self.model.filePath(index))

    def keyPressEvent(self, event):
        index = self.selectionModel().currentIndex()
        if event.key() == QtCore.Qt.Key_Up:
            self.selectionModel().setCurrentIndex(index.sibling(index.row() - 1, index.column()),
                                         QtGui.QItemSelectionModel.SelectCurrent)
        elif event.key() == QtCore.Qt.Key_Down:
            self.selectionModel().setCurrentIndex(index.sibling(index.row() + 1, index.column()),
                                         QtGui.QItemSelectionModel.SelectCurrent)
        elif event.key() == QtCore.Qt.Key_Right:
            self.selectionsview.add_selection(str(self.model.filePath(self.selectionModel().currentIndex())))
        elif event.key() == QtCore.Qt.Key_Left:
            self.selectionsview_1.add_selection(str(self.model.filePath(self.selectionModel().currentIndex())))


class InteractiveFileBrowser(QtGui.QWidget):
    def __init__(self, selectionsview_1, selectionsview, plotview, parent=None):
        super(InteractiveFileBrowser, self).__init__(parent)

        h_layout = QtGui.QHBoxLayout()

        self.type_dir = QtGui.QLineEdit()
        h_layout.addWidget(self.type_dir, 1)

        self.go_to_dir_button = QtGui.QPushButton('go to')
        h_layout.addWidget(self.go_to_dir_button)

        v_layout = QtGui.QVBoxLayout()
        v_layout.addLayout(h_layout)

        self.fileview = FileView(selectionsview_1, selectionsview, plotview)

        v_layout.addWidget(self.fileview)

        self.setLayout(v_layout)

        self.go_to_dir_button.clicked.connect(self.go_to_dir)

    def go_to_dir(self, _):
        self.fileview.setRootIndex(self.fileview.model.index(self.type_dir.text()))
