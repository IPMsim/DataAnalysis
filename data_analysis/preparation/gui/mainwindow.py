from fileviewer import InteractiveFileBrowser
from plotviewer import MiniPlot2D
from selectionviewer import SelectionView
from PyQt4 import QtGui, QtCore


class MainWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__()

        self.selectionview_1 = SelectionView()
        self.selectionview = SelectionView()
        self.plotview = MiniPlot2D()
        self.fileview = InteractiveFileBrowser(self.selectionview_1, self.selectionview, self.plotview)

        h_splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        h_splitter.addWidget(self.selectionview_1)
        h_splitter.addWidget(self.fileview)
        h_splitter.addWidget(self.selectionview)
        h_splitter.addWidget(self.plotview)

        h_layout = QtGui.QHBoxLayout()
        h_layout.addWidget(h_splitter)
        self.setLayout(h_layout)
