import os.path
from PyQt4 import QtGui, QtCore


class FileView(QtGui.QTreeView):
    file_selected = QtCore.pyqtSignal('QString')

    def __init__(self, parent=None):
        super(FileView, self).__init__(parent)

        root_dir = '/'

        self.model = QtGui.QFileSystemModel()
        self.model.setRootPath(root_dir)
        self.model.setNameFilters(['*.dat'])
        self.model.setNameFilterDisables(False)
        self.setModel(self.model)

        self.setRootIndex(self.model.index(root_dir))

        self.setSelectionModel(QtGui.QItemSelectionModel(self.model))


class InteractiveFileBrowser(QtGui.QWidget):
    def __init__(self, parent=None):
        super(InteractiveFileBrowser, self).__init__(parent)

        h_layout = QtGui.QHBoxLayout()

        self.type_dir = QtGui.QLineEdit()
        h_layout.addWidget(self.type_dir, 1)

        self.go_to_dir_button = QtGui.QPushButton('go to')
        h_layout.addWidget(self.go_to_dir_button)

        v_layout = QtGui.QVBoxLayout()
        v_layout.addLayout(h_layout)

        self.fileview = FileView()

        v_layout.addWidget(self.fileview)

        self.setLayout(v_layout)

        self.go_to_dir_button.clicked.connect(self.go_to_dir)

    def go_to_dir(self, _):
        self.fileview.setRootIndex(self.fileview.model.index(self.type_dir.text()))

    def selectionModel(self):
        return self.fileview.selectionModel()

    @property
    def model(self):
        return self.fileview.model
