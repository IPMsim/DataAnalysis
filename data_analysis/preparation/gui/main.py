import sys
from mainwindow import MainWindow
from mainwindow1d import MainWindow1D
from PyQt4 import QtGui
import sys

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    if len(sys.argv) > 1 and sys.argv[1] == '1D':
        view = MainWindow1D()
    else:
        view = MainWindow()
    view.show()

    sys.exit(app.exec_())
