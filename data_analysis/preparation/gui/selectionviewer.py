import datetime
import logging
import os
from PyQt4 import QtGui, QtCore
import re
import shutil
import time

logging.basicConfig(level=logging.DEBUG)


class SelectionView(QtGui.QWidget):
    def __init__(self, parent=None):
        super(SelectionView, self).__init__(parent)

        self.selections = QtGui.QListWidget()
        self.prefix = QtGui.QLineEdit()
        self.prefix.setText('images')
        self.prefix.setReadOnly(True)

        h_layout = QtGui.QHBoxLayout()
        h_layout.addWidget(QtGui.QLabel('Folder:'))
        h_layout.addWidget(self.prefix)

        save_button = QtGui.QPushButton('Save')
        save_button.clicked.connect(self.save)

        v_layout = QtGui.QVBoxLayout()
        v_layout.addLayout(h_layout)
        v_layout.addWidget(self.selections)
        v_layout.addWidget(save_button)
        self.setLayout(v_layout)

        self.filepaths = {}

    def add_selection(self, filepath):
        filename = os.path.split(filepath)[-1]
        self.selections.addItem(filename)
        self.filepaths[filename] = filepath

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            current_item = self.selections.currentItem()
            self.selections.takeItem(self.selections.row(current_item))
            self.filepaths[str(current_item.text())] = None

    def save(self):
        dialog = FolderDialog()
        if dialog.exec_():
            folder = dialog.get_input()
        else:
            return
        logging.debug('mkdir %s' % folder)
        try:
            os.mkdir(folder)
        except OSError:
            logging.debug('%s already exists' % folder)
        prefix = str(self.prefix.text())
        os.mkdir(os.path.join(folder, prefix))
        regex_pattern = 'ipmImg-(?P<date>[0-9]+)_(?P<time>[0-9]+)_(?P<millis>[0-9]+)_[_a-zA-Z0-9]+\.png'
        datetime_format = "%Y-%m-%d %H:%M:%S"
        profile2d_tags = []
        i = 1
        for filename, filepath in iter(self.filepaths.items()):
            if filepath is None:
                continue
            logging.debug('%s cp-> %s' % (filepath, os.path.join(folder, prefix, filename)))
            shutil.copy(filepath, os.path.join(folder, prefix, filename))
            match = re.match(regex_pattern, filename)
            if match is None:
                raise ValueError('Datetime regex did not match')
            gd = match.groupdict()
            datetime_string = '%s-%s-%s %s:%s:%s' % (gd['date'][:4], gd['date'][4:6], gd['date'][6:8],
                                                     gd['time'][:2], gd['time'][2:4], gd['time'][4:6])
            # print 'Datetime string: %s' % datetime_string
            unix_time = (time.mktime(datetime.datetime.strptime(datetime_string, datetime_format).timetuple())
                         + float(gd['millis']) / 1000.)
            # print '%.3f' % unix_time
            unix_time_str = '%.3f' % unix_time
            timestamp_tag = self.write_tag_and_text('Timestamp', unix_time_str, {'unit': '"s"'})
            position_tag = self.write_tag_and_text('Position', '0.0755*i + 0 (1280)', {'unit': '"mm"'})
            filename_tag = self.write_tag_and_text('Filename', os.path.join(prefix, filename), {})
            profile2d_tag = self.write_tag_and_text_indentation('Profile2D', '\n'.join([timestamp_tag, position_tag,
                                                                                        filename_tag]),
                                                                {'id': '"%d"' % i, 'type': '"measured"'})
            profile2d_tags.append(profile2d_tag)
            i += 1
        profile2d_tags.append(self.write_tag_and_text('Comment', '', {}))

        profileset_tag = self.write_tag_and_text_indentation('ProfileSet', '\n'.join(profile2d_tags), {})
        with open(os.path.join(folder, 'output.xml'), 'w') as fp:
            fp.write(profileset_tag)

    @staticmethod
    def write_tag_and_text(tag, text, attributes, level=0):
        indent = level * '    '
        attribute_string = SelectionView.get_attribute_string(attributes)
        return indent + ('<%s%s>%s</%s>' % (tag, attribute_string, text, tag))

    @staticmethod
    def write_tag_and_text_indentation(tag, text, attributes, level=0):
        indent = level * '    '
        more_indent = indent + '    '
        attribute_string = SelectionView.get_attribute_string(attributes)
        return '%s<%s%s>\n%s\n%s</%s>' % (indent, tag, attribute_string,
                                          '\n'.join([more_indent + l for l in text.split('\n')]), indent, tag)

    @staticmethod
    def get_attribute_string(attributes):
        attribute_string = ' '.join(['%s=%s' % (k, v) for k, v in iter(attributes.items())])
        if attribute_string:
            attribute_string = ' ' + attribute_string
        return attribute_string


class FolderDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(FolderDialog, self).__init__(parent)

        self.folder_input = QtGui.QLineEdit()
        ok_button = QtGui.QPushButton('ok')
        cancel_button = QtGui.QPushButton('cancel')

        v_layout = QtGui.QVBoxLayout()

        h_layout = QtGui.QHBoxLayout()
        h_layout.addWidget(QtGui.QLabel('Folder:'))
        h_layout.addWidget(self.folder_input)

        v_layout.addLayout(h_layout)

        h_layout = QtGui.QHBoxLayout()
        h_layout.addStretch(1)
        h_layout.addWidget(ok_button)
        h_layout.addWidget(cancel_button)

        v_layout.addLayout(h_layout)

        self.setLayout(v_layout)

        ok_button.clicked.connect(self.accept)
        cancel_button.clicked.connect(self.reject)

    def get_input(self):
        return str(self.folder_input.text())
