from collections import defaultdict
import datetime
import logging
import numpy as np
import re
import sys
import time

from xml_utils import create_xml_1d

logging.basicConfig(level=logging.DEBUG)

assert len(sys.argv) == 3, 'Not enough arguemnts'

file_in = sys.argv[1]
file_out = sys.argv[2]

with open(file_in) as fp:
    text = fp.read()

pattern = r"\<Profile\>\<No\>(?P<nr>[0-9]+)\</No\>\<ProfileReadoutTime_yyyyMMdd_hhmmss_zzz\>(?P<date>[0-9]+)_(?P<time>[0-9]+)_(?P<millis>[0-9]+)\</ProfileReadoutStartTime\>\<ProfileData\>(?P<values>[.0-9;\s]+)\</ProfileData\>"

profiles = list(re.finditer(pattern, text))
print 'number of profiles: %d' % len(profiles)

nrs = [int(m.groupdict()['nr']) for m in profiles]

print 'Min profile id: %d, max profile id: %d' % (min(nrs), max(nrs))
print 'Profile ids without duplicates: %d' % len(list(set(nrs)))
print 'Missing numbers: %s' % str(set(range(max(nrs))) - set(nrs))

duplicate_profiles = defaultdict(list)

for match in profiles:
    if nrs.count(int(match.groupdict()['nr'])) > 1:
        duplicate_profiles[int(match.groupdict()['nr'])].append(str(match.groupdict()))

print duplicate_profiles.keys()

for nr, duplicates in iter(duplicate_profiles.items()):
    print '%d (%d): is duplicate: %s' % (nr, len(duplicates), str(duplicates.count(duplicates[0]) == len(duplicates)))
quit()


datetime_format = "%Y-%m-%d %H:%M:%S"
timestamps = []
ys = []
for profile in profiles:
    date_s = profile.groupdict()['date']
    time_s = profile.groupdict()['time']
    millis = profile.groupdict()['millis']
    datetime_string = '%s-%s-%s %s:%s:%s' % (date_s[:4], date_s[4:6], date_s[6:8], time_s[:2], time_s[2:4], time_s[4:6])
    unix_time_str = '%.0f.%s' % (time.mktime(datetime.datetime.strptime(datetime_string, datetime_format).timetuple()),
                                 millis)
    timestamps.append(unix_time_str)

    values = profile.groupdict()['values']
    ys.append(filter(None, values.split(';')))

xml_code = create_xml_1d(ys, '0.0755*i + 0', timestamps, None)

with open(file_out, 'w') as fp:
    fp.write(xml_code)
