import datetime
import glob
import logging
import numpy as np
import os
import shutil
from subprocess import call
import sys
import time
from xml_utils import create_xml_1d


logging.basicConfig(level=logging.DEBUG)


def get_rows_from_file(filepath, delim=' ', skip='#', skip_start=0, cast_to=str):
    with open(filepath) as fp:
        lines = fp.read().strip().split('\n')
        return [[cast_to(e) for e in l.split(delim)] for l in lines[skip_start:]
                if len(l) > 0 and not l.startswith(skip)]


def get_columns_from_file(*args, **kwargs):
    return zip(*get_rows_from_file(*args, **kwargs))


def extract_and_rename(filepath):
    head, tail = os.path.split(filepath)
    name, suffix = os.path.splitext(tail)

    new_head = os.path.join(head, name)
    logging.debug('mkdir %s' % new_head)
    os.mkdir(new_head)
    new_filepath = os.path.join(new_head, tail)
    logging.debug('%s -> %s' % (filepath, new_filepath))
    shutil.move(filepath, new_filepath)

    tar_command = ['tar', 'xzf', new_filepath, '-C', new_head]
    logging.debug(' '.join(tar_command))
    call(tar_command)

    adc_path = os.path.join(new_head, 'adc_clean.dat')
    new_adc_path = os.path.join(new_head, '%s.dat' % name)
    logging.debug('%s -> %s' % (adc_path, new_adc_path))
    shutil.move(adc_path, new_adc_path)

    return new_adc_path


def get_unix_timestamp(filepath):
    head, tail = os.path.split(filepath)
    name, suffix = os.path.splitext(tail)

    date_s, time_s = name.split('_')
    logging.debug('date: %s, time: %s' % (date_s, time_s))
    datetime_string = '20%s-%s-%s %s:%s:%s' % (date_s[:2], date_s[2:4], date_s[4:6],
                                               time_s[:2], time_s[2:4], time_s[4:6])
    datetime_format = "%Y-%m-%d %H:%M:%S"
    unix_time = (time.mktime(datetime.datetime.strptime(datetime_string, datetime_format).timetuple()))
    logging.debug('unix time: %.3f' % unix_time)

    return unix_time


def get_profiles_from_file(filepath):
    n_wires = 64
    logging.debug('Extracting profiles from %s' % filepath)
    nrs, ys_h, ys_v = get_columns_from_file(filepath, delim='  ', skip_start=1, cast_to=str)
    assert len(ys_h) % n_wires == 0, 'Number of data points is not a multiple of %s' % n_wires
    ys_h, ys_v = (np.array(ys_h).reshape(len(ys_h) / n_wires, n_wires),
                  np.array(ys_v).reshape(len(ys_v) / n_wires, n_wires))
    return ys_h, ys_v


def read_xml_template(filepath):
    with open(filepath) as fp:
        template = fp.read()
    return template


def write_xml(filepath_of_source, orientation, xml_template, xml_profileset):
    head, tail = os.path.split(filepath_of_source)
    name, suffix = os.path.splitext(tail)

    xml_file = os.path.join(head, '%s_%s.xml' % (name, orientation))
    logging.debug(xml_file)
    with open(xml_file, 'w') as fp:
        fp.write(xml_template % xml_profileset)

    return xml_file


def copy_xml_to_destination(source, destination_dir):
    _, tail = os.path.split(source)
    new_filepath = os.path.join(destination_dir, tail)
    logging.debug('%s cp-> %s' % (source, new_filepath))
    shutil.copy(source, new_filepath)


assert len(sys.argv) == 4, 'Not enough arguments'

# extract_and_rename(sys.argv[1])


directory = sys.argv[1]
copy_to_dir_h = sys.argv[2]
copy_to_dir_v = sys.argv[3]
template_h_filepath = '/home/dominik/Downloads/Measurement_2016-06-28/template_old_horizontal.xml'
template_v_filepath = '/home/dominik/Downloads/Measurement_2016-06-28/template_old_vertical.xml'

positions = ''
time_difference_between_profiles = 10

template_h = read_xml_template(template_h_filepath)
template_v = read_xml_template(template_v_filepath)

for filepath in glob.glob(os.path.join(directory, '*.tgz')):
    new_filepath = extract_and_rename(filepath)
    unix_time = get_unix_timestamp(new_filepath)
    ys_h, ys_v = get_profiles_from_file(new_filepath)

    xml_profileset_h = create_xml_1d(ys_h, positions, unix_time, time_difference_between_profiles)
    xml_file_h = write_xml(new_filepath, 'h', template_h, xml_profileset_h)
    copy_xml_to_destination(xml_file_h, copy_to_dir_h)

    xml_profileset_v = create_xml_1d(ys_v, positions, unix_time, time_difference_between_profiles)
    xml_file_v = write_xml(new_filepath, 'v', template_v, xml_profileset_v)
    copy_xml_to_destination(xml_file_v, copy_to_dir_v)
