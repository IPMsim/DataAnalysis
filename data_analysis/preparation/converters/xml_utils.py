import logging


def write_tag_and_text(tag, text, attributes, level=0):
    indent = level * '    '
    attribute_string = get_attribute_string(attributes)
    return indent + ('<%s%s>%s</%s>' % (tag, attribute_string, text, tag))


def write_tag_and_text_indentation(tag, text, attributes, level=0):
    indent = level * '    '
    more_indent = indent + '    '
    attribute_string = get_attribute_string(attributes)
    return '%s<%s%s>\n%s\n%s</%s>' % (indent, tag, attribute_string,
                                      '\n'.join([more_indent + l for l in text.split('\n')]), indent, tag)


def get_attribute_string(attributes):
    attribute_string = ' '.join(['%s=%s' % (k, v) for k, v in iter(attributes.items())])
    if attribute_string:
        attribute_string = ' ' + attribute_string
    return attribute_string


def create_xml_1d(ys, positions, unix_time, time_difference):
    """

    :param ys: np.array, containing values as strings
    :param positions: string, values comma separated
    :param unix_time: float [s]
    :param time_difference: [ms]
    :return: xml_code
    """
    if isinstance(unix_time, float) or isinstance(unix_time, int):
        unix_time_strings = ['%.3f' % (unix_time + ((i - 1) * time_difference) / 1000.) for i in range(len(ys))]
    elif isinstance(unix_time, list):
        assert len(unix_time) == len(ys), 'Number of unix timestamps does not match number of profiles'
        unix_time_strings = map(str, unix_time)
    else:
        raise TypeError('Wrong type for unix_time: %s' % type(unix_time))

    profile_elements = []
    i = 1
    for y in ys:
        unix_time_str = unix_time_strings[i-1]
        timestamp_tag = write_tag_and_text('Timestamp', unix_time_str, {'unit': '"s"'})
        position_tag = write_tag_and_text('Position', positions, {'unit': '"mm"'})
        amplitude_tag = write_tag_and_text('Amplitude', ','.join(y), {'unit': '"arbitrary"'})
        profile_tag = write_tag_and_text_indentation('Profile', '\n'.join([timestamp_tag, position_tag, amplitude_tag]),
                                                     {'id': '"%d"' % i, 'type': '"measured"'})
        profile_elements.append(profile_tag)
        i += 1
    profile_elements.append(write_tag_and_text('Comment', '', {}))
    profileset_tag = write_tag_and_text_indentation('ProfileSet', '\n'.join(profile_elements), {}, level=1)
    return profileset_tag


def create_xml_2d(filenames, unix_times):
    assert len(filenames) == len(unix_times), 'Number of filenames does not match number of unix times'
    profile2d_tags = []
    for i, filename, unix_time in zip(range(len(filenames)), filenames, unix_times):
        unix_time_str = '%.3f' % unix_time
        timestamp_tag = write_tag_and_text('Timestamp', unix_time_str, {'unit': '"s"'})
        position_tag = write_tag_and_text('Position', '0.0755*i + 0 (1280)', {'unit': '"mm"'})
        filename_tag = write_tag_and_text('Filename', filename, {})
        profile2d_tag = write_tag_and_text_indentation('Profile2D', '\n'.join([timestamp_tag, position_tag,
                                                                                    filename_tag]),
                                                            {'id': '"%d"' % (i+1), 'type': '"measured"'})
        profile2d_tags.append(profile2d_tag)

    # profile2d_tags.append(write_tag_and_text('Comment', '', {}))
    profileset_tag = write_tag_and_text_indentation('ProfileSet', '\n'.join(profile2d_tags), {}, level=1)
    return profileset_tag


def save_profile_set_with_template(destination_filepath, template_filepath, profile_set):
    logging.debug('Loading template from %s' % template_filepath)
    with open(template_filepath) as fp:
        template = fp.read()
    logging.debug('Writing XML file to %s' % destination_filepath)
    with open(destination_filepath, 'w') as fp:
        fp.write(template % profile_set)
