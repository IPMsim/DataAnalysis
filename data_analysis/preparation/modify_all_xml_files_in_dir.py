import os
import sys


def prompt_yes():
    choice = raw_input('OK? [Y/n] ')
    if choice != 'Y':
        print 'quit'
        quit()


def replace_all(directory, files):
    assert len(sys.argv) >= 4, 'Not enough arguments'

    old = sys.argv[2]
    new = sys.argv[3]

    print 'Applying modification'
    print "'%s'" % old
    print '=>'
    print "'%s'" % new
    print "to %d file(s) in directory '%s'" % (len(files), directory)
    prompt_yes()

    for f in files:
        replace(f, old, new)


def replace(filename, old, new):
    with open(filename) as fp:
        content = fp.read()
    content = content.replace(old, new)
    with open(filename, 'w') as fp:
        fp.write(content)


def embed_all(directory, files):
    assert len(sys.argv) >= 3, 'Not enough arguments'

    template_filename = sys.argv[2]

    print 'Embedding %d files' % len(files)
    print "in directory '%s'" % directory
    print "into template '%s'" % template_filename
    prompt_yes()

    for f in files:
        embed_profileset_into_template(f, template_filename)


def embed_profileset_into_template(profileset_filename, template_filename):
    with open(template_filename) as fp:
        template = fp.read()
    with open(profileset_filename) as fp:
        profileset = fp.read()
    with open(profileset_filename, 'w') as fp:
        fp.write(template % profileset)


if __name__ == '__main__':
    assert len(sys.argv) >= 2, 'Not enough arguments'

    directory = sys.argv[1]

    files = [os.path.join(d[0], f) for d in list(os.walk(directory)) for f in d[-1]]
    files = filter(lambda f: os.path.splitext(f)[-1] == '.xml', files)

    replace_all(directory, files)
