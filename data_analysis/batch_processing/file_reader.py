import os
import re
import xml.etree.ElementTree as XMLET

import numpy as np

from data_analysis.tools.ipm_profiles import Profile1D


class BaseReader(object):
    def __init__(self):
        self.suffix = ''

    @property
    def pattern(self):
        return '*' + self.suffix


class XMLReader(BaseReader):
    def __init__(self, delimiter):
        super(XMLReader, self).__init__()

        self.suffix = '.xml'
        self.delimiter = delimiter

    def extract_profiles_from_file(self, filepath):
        XMLReader.prepare_data_file_gsi(filepath)

        dom_tree = XMLET.parse(filepath)

        pixel_per_millimeter = float(dom_tree.find('Header/PixScale').text)

        profiles_xml = dom_tree.findall('DATA/Profile')
        profiles = []
        for profile in profiles_xml:
            ys = map(float, filter(None, profile.find('ProfileData').text.split(self.delimiter)))
            xs = np.array(range(len(ys))) / pixel_per_millimeter                              # [mm]
            profiles.append(Profile1D(XMLReader._create_profile_id(filepath, profile), xs, ys))

        return profiles

    @staticmethod
    def _create_profile_id(filepath, profile):
        return os.path.splitext(os.path.basename(filepath))[0] + '_no' + profile.find('No').text

    @staticmethod
    def prepare_data_file_gsi(filepath):
        with open(filepath) as fp:
            content = fp.read()

        if content[-1] != '\n':
            content += '\n'

        xml_version_pattern = '\<\?xml version="[\.0-9]+"\?\>'
        if not re.match(xml_version_pattern, content.split('\n')[0]):  # check if xml file has already valid header
            content = '<?xml version="1.0"?>\n' + '<Measurement>\n' + content + '</Measurement>\n'

        with open(filepath, 'w') as fp:
            fp.write(content)
