from setuptools import setup

setup(
    name='data_analysis',
    version='0.0.1',
    description='This package contains various tools for data analysis and data visualization.',
    author='Dominik Vilsmeier',
    author_email='d.vilsmeier@gsi.de',
    license='MIT',
    packages=['data_analysis.gui', 'data_analysis.tools'],
    scripts=['bin/xml_gui'],
    zip_safe=False
)
